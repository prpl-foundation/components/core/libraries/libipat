/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <ipat/ipat.h>

char* ipat_text_copy(const char* ipt, bool cidr) {
    ipat_t dst = IPAT_INITIALIZE, src = IPAT_INITIALIZE;

    ipat_pton(&src, AF_INET_ANY, ipt);
    if(ipat_copy(&dst, &src)) {
        return ipat_ntop_alloc(&dst, cidr);
    }

    return NULL;
}

bool ipat_text_valid(const char* ipt) {
    ipat_t ip = IPAT_INITIALIZE;

    return ipat_pton(&ip, AF_INET_ANY, ipt);
}

char* ipat_text_set(int family, const void* src, int srclen, bool cidr) {
    ipat_t ip = IPAT_INITIALIZE;

    if(ipat_set(&ip, family, src, srclen)) {
        return ipat_ntop_alloc(&ip, cidr);
    }

    return NULL;
}

char* ipat_text_set_nul(int family, bool cidr) {
    ipat_t ip = IPAT_INITIALIZE;

    if(ipat_set_nul(&ip, family)) {
        return ipat_ntop_alloc(&ip, cidr);
    }

    return NULL;
}

char* ipat_text_set_any(int family) {
    ipat_t ip = IPAT_INITIALIZE;

    if(ipat_set_any(&ip, family)) {
        return ipat_ntop_alloc(&ip, false);
    }

    return NULL;
}

bool ipat_text_is_any(const char* ipt) {
    ipat_t ip = IPAT_INITIALIZE;

    ipat_pton(&ip, AF_INET_ANY, ipt);
    return ipat_is_any(&ip);
}

int ipat_text_family(const char* ipt) {
    ipat_t ip = IPAT_INITIALIZE;

    ipat_pton(&ip, AF_INET_ANY, ipt);
    return ipat_family(&ip);
}

void* ipat_text_buffer(const char* ipt, int* size) {
    ipat_t ip = IPAT_INITIALIZE;

    ipat_pton(&ip, AF_INET_ANY, ipt);
    return ipat_buffer(&ip, size);
}

int ipat_text_prefix_length(const char* ipt) {
    ipat_t ip = IPAT_INITIALIZE;

    ipat_pton(&ip, AF_INET_ANY, ipt);
    return ipat_prefix_length(&ip);
}

char* ipat_text_set_prefix_length(const char* ipt, int prefixlen, bool cidr) {
    ipat_t ip = IPAT_INITIALIZE;

    ipat_pton(&ip, AF_INET_ANY, ipt);
    if(ipat_set_prefix_length(&ip, prefixlen)) {
        return ipat_ntop_alloc(&ip, cidr);
    }

    return NULL;
}

unsigned int ipat_text_type(const char* ipt, unsigned int* family) {
    ipat_t ip = IPAT_INITIALIZE;

    ipat_pton(&ip, AF_INET_ANY, ipt);
    return ipat_type(&ip, family);
}

char* ipat_text_mask_create(int family, int len, ipat_bits_t bits, bool cidr) {
    ipat_t ip = IPAT_INITIALIZE;

    ipat_mask_create(&ip, family, len, bits);
    return ipat_ntop_alloc(&ip, cidr);
}

char* ipat_text_mask_apply(const char* ipt, const char* mask, ipat_oper_t oper, bool cidr) {
    ipat_t ip = IPAT_INITIALIZE, ip_mask = IPAT_INITIALIZE;

    ipat_pton(&ip, AF_INET_ANY, ipt);
    ipat_pton(&ip_mask, AF_INET_ANY, mask);
    if(ipat_mask_apply(&ip, &ip_mask, oper)) {
        return ipat_ntop_alloc(&ip, cidr);
    }

    return NULL;
}

char* ipat_text_mask_apply_direct(const char* ipt, int len, ipat_bits_t bits, ipat_oper_t oper, bool cidr) {
    ipat_t ip = IPAT_INITIALIZE;

    ipat_pton(&ip, AF_INET_ANY, ipt);
    if(ipat_mask_apply_direct(&ip, len, bits, oper)) {
        return ipat_ntop_alloc(&ip, cidr);
    }

    return NULL;
}

char* ipat_text_to_subnet(const char* ipt) {
    ipat_t ip = IPAT_INITIALIZE, subnet = IPAT_INITIALIZE;

    ipat_pton(&ip, AF_INET_ANY, ipt);
    if(ipat_to_subnet(&subnet, &ip)) {
        return ipat_ntop_alloc(&subnet, true);
    }

    return NULL;
}

int ipat_text_compare(const char* a, const char* b, bool prefixlen) {
    ipat_t ip_a = IPAT_INITIALIZE, ip_b = IPAT_INITIALIZE;

    ipat_pton(&ip_a, AF_INET_ANY, a);
    ipat_pton(&ip_b, AF_INET_ANY, b);
    return ipat_compare(&ip_a, &ip_b, prefixlen);
}

bool ipat_text_in_subnet(const char* ipt, const char* subnet) {
    ipat_t ip = IPAT_INITIALIZE, ip_subnet = IPAT_INITIALIZE;

    ipat_pton(&ip, AF_INET_ANY, ipt);
    ipat_pton(&ip_subnet, AF_INET_ANY, subnet);
    return ipat_in_subnet(&ip, &ip_subnet);
}

char* ipat_text_make_EUI64(const char* ipt, const char* mac, bool cidr) {
    ipat_t ip = IPAT_INITIALIZE;

    ipat_pton(&ip, AF_INET_ANY, ipt);
    if(ipat_make_EUI64_text(&ip, mac)) {
        return ipat_ntop_alloc(&ip, cidr);
    }

    return NULL;
}

bool ipat_text_is_EUI64(const char* ipt, const char* mac) {
    ipat_t ip = IPAT_INITIALIZE;

    ipat_pton(&ip, AF_INET_ANY, ipt);
    return ipat_is_EUI64_text(&ip, mac);
}

char* ipat_text_convert(const char* ipt, int to_family, bool cidr) {
    ipat_t ip = IPAT_INITIALIZE;

    ipat_pton(&ip, AF_INET_ANY, ipt);
    if(ipat_convert(&ip, to_family)) {
        return ipat_ntop_alloc(&ip, cidr);
    }

    return NULL;
}

