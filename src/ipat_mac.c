/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <ipat/ipat_mac.h>

#include "ipat_buffer.h"

static char ipat_mac_separator(ipat_mac_sep_t sep) {
    switch(sep) {
    case ipat_mac_sep_colon:
        return ':';
        break;
    case ipat_mac_sep_dash:
        return '-';
        break;
    case ipat_mac_sep_underscore:
        return '_';
        break;
    case ipat_mac_sep_none:
    default:
        break;
    }

    return '\0';
}

bool ipat_mac_initialize(ipat_mac_t* mac) {
    if(!mac) {
        errno = EINVAL;
        return false;
    }

    memset(mac, 0, sizeof(ipat_mac_t));

    return true;
}

void ipat_mac_cleanup(ipat_mac_t* mac) {
    if(mac) {
        memset(mac, 0, sizeof(ipat_mac_t));
    }
}

bool ipat_mac_copy(ipat_mac_t* dst, const ipat_mac_t* src) {
    if(!dst || !src) {
        errno = EINVAL;
        return false;
    }

    memcpy(dst, src, sizeof(ipat_mac_t));

    return true;
}

int ipat_mac_max_size(ipat_mac_part_t part) {
    switch(part) {
    case ipat_mac_part_full:
        return IPAT_SIZE_MAC;
        break;
    case ipat_mac_part_oui:
    case ipat_mac_part_nic:
        return IPAT_SIZE_MAC_PART;
        break;
    default:
        break;
    }

    errno = EINVAL;
    return -1;
}

int ipat_mac_min_string_length(ipat_mac_sep_t sep) {
    if(sep == ipat_mac_sep_none) {
        return IPAT_STRLEN_MAC_COMPACT;
    }

    return IPAT_STRLEN_MAC;
}

bool ipat_mac_valid_separator(char csep, ipat_mac_sep_t* sep) {
    ipat_mac_sep_t res = ipat_mac_sep_none;

    switch(csep) {
    case '\0':
        res = ipat_mac_sep_none;
        break;
    case ':':
        res = ipat_mac_sep_colon;
        break;
    case '-':
        res = ipat_mac_sep_dash;
        break;
    case '_':
        res = ipat_mac_sep_underscore;
        break;
    default:
        return false;
        break;
    }

    if(sep) {
        *sep = res;
    }

    return true;
}

bool ipat_mac_valid(const ipat_mac_t* mac) {
    if(!mac) {
        errno = EINVAL;
        return false;
    }

    return mac->valid;
}

bool ipat_mac_pton(ipat_mac_t* mac, const char* src) {
    if(!mac || !src) {
        errno = EINVAL;
        return false;
    }

    mac->valid = ipat_buffer_fill_mac(mac->buffer, IPAT_SIZE_MAC, src);

    return mac->valid;
}

char* ipat_mac_ntop(const ipat_mac_t* mac, char* dst, int dstlen, ipat_mac_sep_t sep, bool upper) {
    int minstrlen = 0;
    char format[50] = "";

    if(!mac || !mac->valid || !dst) {
        errno = EINVAL;
        return NULL;
    }

    minstrlen = ipat_mac_min_string_length(sep);
    if((minstrlen == -1) || (minstrlen > dstlen)) {
        errno = EINVAL;
        return NULL;
    }

    if(sep == ipat_mac_sep_none) {
        if(upper) {
            strcpy(format, "%02X%02X%02X%02X%02X%02X");
        } else {
            strcpy(format, "%02x%02x%02x%02x%02x%02x");
        }
    } else {
        if(upper) {
            strcpy(format, "%02Xs%02Xs%02Xs%02Xs%02Xs%02X");
        } else {
            strcpy(format, "%02xs%02xs%02xs%02xs%02xs%02x");
        }

        format[4] = ipat_mac_separator(sep);
        format[9] = ipat_mac_separator(sep);
        format[14] = ipat_mac_separator(sep);
        format[19] = ipat_mac_separator(sep);
        format[24] = ipat_mac_separator(sep);
    }

    snprintf(dst, dstlen, format, mac->buffer[0], mac->buffer[1], mac->buffer[2], mac->buffer[3], mac->buffer[4], mac->buffer[5]);

    return dst;
}

char* ipat_mac_ntop_alloc(const ipat_mac_t* mac, ipat_mac_sep_t sep, bool upper) {
    int dstlen = 0;
    char* dst = NULL, * ret = NULL;

    if(!mac || !mac->valid) {
        errno = EINVAL;
        return NULL;
    }

    dst = calloc(1, IPAT_STRLEN_MAC_SAFE);
    if(!dst) {
        errno = ENOMEM;
        return false;
    }
    dstlen = IPAT_STRLEN_MAC_SAFE;

    ret = ipat_mac_ntop(mac, dst, dstlen, sep, upper);
    if(!ret) {
        free(dst);
    }

    return ret;
}

void* ipat_mac_buffer(const ipat_mac_t* mac) {
    int maxsize = 0;
    unsigned char* buffer = NULL;

    if(!mac || !mac->valid) {
        errno = EINVAL;
        return NULL;
    }

    maxsize = ipat_mac_max_size(ipat_mac_part_full);
    if(maxsize == -1) {
        errno = EINVAL;
        return NULL;
    }

    buffer = calloc(1, maxsize);
    if(!buffer) {
        errno = ENOMEM;
        return NULL;
    }

    memcpy(buffer, mac->buffer, maxsize);

    return buffer;
}

void* ipat_mac_da_buffer(ipat_mac_t* mac) {
    if(!mac || !mac->valid) {
        errno = EINVAL;
        return NULL;
    }

    return (void*) mac->buffer;
}

bool ipat_mac_set(ipat_mac_t* mac, const void* src, int srclen) {
    return ipat_mac_set_part(mac, src, srclen, ipat_mac_part_full);
}

bool ipat_mac_set_nul(ipat_mac_t* mac) {
    if(!mac) {
        errno = EINVAL;
        return false;
    }

    memset(mac->buffer, 0, IPAT_SIZE_MAC);
    mac->valid = true;

    return true;
}

bool ipat_mac_get_part(const ipat_mac_t* mac, void* dst, int dstlen, ipat_mac_part_t part) {
    int maxsize = 0;

    if(!mac || !mac->valid || !dst) {
        errno = EINVAL;
        return false;
    }

    maxsize = ipat_mac_max_size(part);
    if((maxsize == -1) || (maxsize > dstlen)) {
        errno = EINVAL;
        return false;
    }

    if(part == ipat_mac_part_nic) {
        memcpy(dst, mac->buffer + maxsize, maxsize);
    } else {
        memcpy(dst, mac->buffer, maxsize);
    }

    return true;
}

bool ipat_mac_set_part(ipat_mac_t* mac, const void* src, int srclen, ipat_mac_part_t part) {
    int maxsize = 0;

    if(!mac || !src) {
        errno = EINVAL;
        return false;
    }

    if((part != ipat_mac_part_full) && !mac->valid) {
        errno = EINVAL;
        return false;
    }

    maxsize = ipat_mac_max_size(part);
    if((maxsize == -1) || (maxsize > srclen)) {
        errno = EINVAL;
        return false;
    }

    if(part == ipat_mac_part_nic) {
        memcpy(mac->buffer + maxsize, src, maxsize);
    } else {
        memcpy(mac->buffer, src, maxsize);
    }

    mac->valid = true;
    return true;
}

bool ipat_mac_is_unicast(const ipat_mac_t* mac) {
    if(!mac || !mac->valid) {
        errno = EINVAL;
        return false;
    }

    return !(mac->buffer[0] & 0x01);
}

bool ipat_mac_set_unicast(ipat_mac_t* mac, bool unicast) {
    if(!mac || !mac->valid) {
        errno = EINVAL;
        return false;
    }

    if(unicast) {
        mac->buffer[0] &= 0xFE;
    } else {
        mac->buffer[0] |= 0x01;
    }

    return true;
}

bool ipat_mac_is_local(const ipat_mac_t* mac) {
    if(!mac || !mac->valid) {
        errno = EINVAL;
        return false;
    }

    return (mac->buffer[0] & 0x02);
}

bool ipat_mac_set_local(ipat_mac_t* mac, bool local) {
    if(!mac || !mac->valid) {
        errno = EINVAL;
        return false;
    }

    if(local) {
        mac->buffer[0] |= 0x02;
    } else {
        mac->buffer[0] &= 0xFD;
    }

    return true;
}

bool ipat_mac_mask_create(ipat_mac_t* mac, int len, ipat_bits_t bits) {
    if(!mac) {
        errno = EINVAL;
        return false;
    }

    mac->valid = ipat_buffer_mask_create(mac->buffer, IPAT_SIZE_MAC, len, bits);

    return mac->valid;
}

bool ipat_mac_mask_apply(ipat_mac_t* mac, const ipat_mac_t* mask, ipat_oper_t oper) {
    if(!mac || !mac->valid || !mask || !mask->valid) {
        errno = EINVAL;
        return false;
    }

    return ipat_buffer_mask_apply(mac->buffer, mask->buffer, IPAT_SIZE_MAC, oper);
}

bool ipat_mac_mask_apply_direct(ipat_mac_t* mac, int len, ipat_bits_t bits, ipat_oper_t oper) {
    unsigned char mask[IPAT_SIZE_MAC];

    if(!mac || !mac->valid) {
        errno = EINVAL;
        return false;
    }

    if(ipat_buffer_mask_create(mask, IPAT_SIZE_MAC, len, bits)) {
        return ipat_buffer_mask_apply(mac->buffer, mask, IPAT_SIZE_MAC, oper);
    }

    return false;
}

int ipat_mac_compare(const ipat_mac_t* a, const ipat_mac_t* b, ipat_mac_part_t part) {
    int maxsize = 0, result = IPAT_COMPARE_ERROR;

    if(!a || !a->valid || !b || !b->valid) {
        errno = EINVAL;
        return IPAT_COMPARE_ERROR;
    }

    maxsize = ipat_mac_max_size(part);
    if(maxsize == -1) {
        errno = EINVAL;
        return IPAT_COMPARE_ERROR;
    }

    if(part == ipat_mac_part_nic) {
        result = memcmp(a->buffer + maxsize, b->buffer + maxsize, maxsize);
    } else {
        result = memcmp(a->buffer, b->buffer, maxsize);
    }

    /* Normalize result */
    if(result < 0) {
        result = IPAT_COMPARE_LESS;
    } else if(result > 0) {
        result = IPAT_COMPARE_GREATER;
    }

    return result;
}

