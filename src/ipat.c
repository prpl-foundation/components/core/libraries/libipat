/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <ipat/ipat.h>

#include "ipat_address.h"
#include "ipat_buffer.h"
#include "ipat_type.h"

static int ipat_mask_prefix_length_family(int family, const char* mask, int* resulting_family) {
    int i;
    bool breakout = false;
    int prefixlen = 0;
    unsigned char buffer[IPAT_SIZE_SAFE];

    if(!ipat_supported_family(family, true)) {
        return -1;
    }

    if((family == AF_INET_ANY) || (family == AF_INET)) {
        if(inet_pton(AF_INET, mask, buffer) == 1) {
            family = AF_INET;
            goto check;
        }
    }

    if((family == AF_INET_ANY) || (family == AF_INET6)) {
        if(inet_pton(AF_INET6, mask, buffer) == 1) {
            family = AF_INET6;
            goto check;
        }
    }

    if(resulting_family) {
        *resulting_family = AF_INET_INVALID;
    }

    errno = EINVAL;
    return -1;

check:
    if(resulting_family) {
        *resulting_family = family;
    }

    for(i = 0; i < ipat_max_size(family) && !breakout; i++) {
        switch(buffer[i]) {
        case 0x00:
            breakout = true;
            break;
        case 0x80:
            prefixlen += 1;
            breakout = true;
            break;
        case 0xC0:
            prefixlen += 2;
            breakout = true;
            break;
        case 0xE0:
            prefixlen += 3;
            breakout = true;
            break;
        case 0xF0:
            prefixlen += 4;
            breakout = true;
            break;
        case 0xF8:
            prefixlen += 5;
            breakout = true;
            break;
        case 0xFC:
            prefixlen += 6;
            breakout = true;
            break;
        case 0xFE:
            prefixlen += 7;
            breakout = true;
            break;
        case 0xFF:
            prefixlen += 8;
            break;
        default:
            errno = EINVAL;
            return -1;
            break;
        }
    }

    for(; i < ipat_max_size(family); i++) {
        if(buffer[i] != 0x00) {
            errno = EINVAL;
            return -1;
        }
    }

    return prefixlen;
}

static bool ipat_pton_any(ipat_t* ip, int family, const char* src) {
    int family_any = AF_INET_INVALID;

    /* Note: Function only called when src starts with at least 'any' */
    switch(src[3]) {
    case 0:
        family_any = AF_INET_ANY;
        break;
    case '4':
        if(src[4] == 0) {
            family_any = AF_INET;
        }
        break;
    case '6':
        if(src[4] == 0) {
            family_any = AF_INET6;
        }
        break;
    default:
        break;
    }
    if(family_any == AF_INET_INVALID) {
        errno = EAFNOSUPPORT;
        return false;
    }

    if((family == AF_INET) || (family == AF_INET_ANY)) {
        if((family_any == AF_INET) || (family_any == AF_INET_ANY)) {
            ip->addr.any = true;
            ip->addr.family = family == AF_INET_ANY ? family_any : family;
            ip->valid = true;

            return true;
        }
    }

    if((family == AF_INET6) || (family == AF_INET_ANY)) {
        if((family_any == AF_INET6) || (family_any == AF_INET_ANY)) {
            ip->addr.any = true;
            ip->addr.family = family == AF_INET_ANY ? family_any : family;
            ip->valid = true;

            return true;
        }
    }

    errno = EINVAL;
    return false;
}

bool ipat_initialize(ipat_t* ip) {
    if(!ip) {
        errno = EINVAL;
        return false;
    }

    memset(ip, 0, sizeof(ipat_t));

    return true;
}

void ipat_cleanup(ipat_t* ip) {
    if(ip) {
        memset(ip, 0, sizeof(ipat_t));
    }
}

bool ipat_copy(ipat_t* dst, const ipat_t* src) {
    if(!dst || !src) {
        errno = EINVAL;
        return false;
    }

    memcpy(dst, src, sizeof(ipat_t));

    return true;
}

int ipat_max_size(int family) {
    switch(family) {
    case AF_INET:
        return IPAT_SIZE_IPV4;
        break;
    case AF_INET6:
        return IPAT_SIZE_IPV6;
        break;
    default:
        break;
    }

    errno = EAFNOSUPPORT;
    return -1;
}

int ipat_max_prefix_length(int family) {
    switch(family) {
    case AF_INET:
        return IPAT_PREFIXLEN_IPV4;
        break;
    case AF_INET6:
        return IPAT_PREFIXLEN_IPV6;
        break;
    default:
        break;
    }

    errno = EAFNOSUPPORT;
    return -1;
}

int ipat_min_string_length(int family, bool cidr) {
    switch(family) {
    case AF_INET:
        return cidr ? IPAT_STRLEN_IPV4_CIDR : IPAT_STRLEN_IPV4;
        break;
    case AF_INET6:
        return cidr ? IPAT_STRLEN_IPV6_CIDR : IPAT_STRLEN_IPV6;
        break;
    default:
        break;
    }

    errno = EAFNOSUPPORT;
    return -1;
}

bool ipat_supported_family(int family, bool allow_any) {
    switch(family) {
    case AF_INET:
    case AF_INET6:
        return true;
        break;
    case AF_INET_ANY:
        if(allow_any) {
            return true;
        }
        break;
    default:
        break;
    }

    errno = EAFNOSUPPORT;
    return false;
}

bool ipat_compatible_family(int family1, int family2) {
    if(!ipat_supported_family(family1, true) || !ipat_supported_family(family2, true)) {
        return false;
    }

    return (family1 == family2 || family1 == AF_INET_ANY || family2 == AF_INET_ANY);
}

int ipat_combine_family(int family1, int family2, ipat_oper_t oper) {
    int bits1 = 0x00, bits2 = 0x00;

    if(((family1 != AF_INET_INVALID) && !ipat_supported_family(family1, true)) ||
       ((family2 != AF_INET_INVALID) && !ipat_supported_family(family2, true))) {
        errno = EAFNOSUPPORT;
        return AF_INET_INVALID;
    }

    bits1 = (family1 == AF_INET ? 0x01 : (family1 == AF_INET6 ? 0x02 : (family1 == AF_INET_ANY ? 0x03 : 0x00)));
    bits2 = (family2 == AF_INET ? 0x01 : (family2 == AF_INET6 ? 0x02 : (family2 == AF_INET_ANY ? 0x03 : 0x00)));

    switch(oper) {
    case ipat_oper_and:
        bits1 &= bits2;
        break;
    case ipat_oper_or:
        bits1 |= bits2;
        break;
    case ipat_oper_xor:
        bits1 ^= bits2;
        break;
    default:
        break;
    }

    return (bits1 == 0x03 ? AF_INET_ANY : (bits1 == 0x02 ? AF_INET6 : (bits1 == 0x01 ? AF_INET : AF_INET_INVALID)));
}

bool ipat_valid(const ipat_t* ip) {
    if(!ip) {
        errno = EINVAL;
        return false;
    }

    return ip->valid;
}

bool ipat_pton(ipat_t* ip, int family, const char* src) {
    char ipat_tmp[IPAT_STRLEN_SAFE] = "";
    int prefixlen = -1;
    char* length = NULL;
    const char* ip_text = NULL;

    if(!ip || !src) {
        errno = EINVAL;
        return false;
    }

    ip->valid = false;

    if(!strncmp(src, ipat_any, strlen(ipat_any))) {
        return ipat_pton_any(ip, family, src);
    }

    if(!ipat_supported_family(family, true)) {
        return false;
    }

    length = strchr(src, '/');
    if(length) {
        char* endptr = NULL;
        int prefix_family = AF_INET_INVALID;

        if((length - src) >= IPAT_STRLEN_SAFE) {
            errno = EINVAL;
            return false;
        }

        if(*(length + 1) == '\0') {
            errno = EINVAL;
            return false;
        }

        errno = 0;
        prefixlen = strtol(length + 1, &endptr, 10);
        if((prefixlen < 0) || (*endptr != '\0') || (errno != 0)) {
            /* It's not a number, so maybe it's an IP-formatted mask? */
            prefixlen = ipat_mask_prefix_length_family(family, length + 1, &prefix_family);
            if(prefixlen == -1) {
                return false;
            }

            /* Force family to that of mask */
            family = prefix_family;
        }

        memset(ipat_tmp, 0, IPAT_STRLEN_SAFE);
        strncpy(ipat_tmp, src, length - src);
        ip_text = ipat_tmp;
    } else {
        ip_text = src;
    }

    if((family == AF_INET_ANY) || (family == AF_INET)) {
        if(inet_pton(AF_INET, ip_text, ip->addr.buffer) == 1) {
            if(prefixlen > IPAT_PREFIXLEN_IPV4) {
                errno = EINVAL;
                return false;
            }

            ip->addr.any = false;
            ip->addr.family = AF_INET;
            ip->addr.prefixlen = prefixlen >= 0 ? prefixlen : IPAT_PREFIXLEN_IPV4;
            ip->valid = true;

            return true;
        }
    }

    if((family == AF_INET_ANY) || (family == AF_INET6)) {
        if(inet_pton(AF_INET6, ip_text, ip->addr.buffer) == 1) {
            if(prefixlen > IPAT_PREFIXLEN_IPV6) {
                errno = EINVAL;
                return false;
            }

            ip->addr.any = false;
            ip->addr.family = AF_INET6;
            ip->addr.prefixlen = prefixlen >= 0 ? prefixlen : IPAT_PREFIXLEN_IPV6;
            ip->valid = true;

            return true;
        }
    }

    errno = EINVAL;
    return false;
}

char* ipat_ntop(const ipat_t* ip, char* dst, int dstlen, bool cidr) {
    int minstrlen = 0;

    if(!ip || !ip->valid || !dst) {
        errno = EINVAL;
        return NULL;
    }

    minstrlen = ip->addr.any ? IPAT_STRLEN_ANY : ipat_min_string_length(ip->addr.family, cidr);
    if((minstrlen == -1) || (minstrlen > dstlen)) {
        errno = EINVAL;
        return NULL;
    }

    if(ip->addr.any) {
        switch(ip->addr.family) {
        case AF_INET_ANY:
            strcpy(dst, ipat_any);
            break;
        case AF_INET:
            strcpy(dst, ipat_any4);
            break;
        case AF_INET6:
            strcpy(dst, ipat_any6);
            break;
        default:
            errno = EAFNOSUPPORT;
            return NULL;
            break;
        }
    } else {
        if(!inet_ntop(ip->addr.family, ip->addr.buffer, dst, dstlen)) {
            return NULL;
        }

        if(cidr) {
            sprintf(dst + strlen(dst), "/%d", ip->addr.prefixlen);
        }
    }

    return dst;
}

char* ipat_ntop_alloc(const ipat_t* ip, bool cidr) {
    int dstlen = 0;
    char* dst = NULL, * ret = NULL;

    if(!ip || !ip->valid) {
        errno = EINVAL;
        return NULL;
    }

    dst = calloc(1, IPAT_STRLEN_SAFE);
    if(!dst) {
        errno = ENOMEM;
        return NULL;
    }
    dstlen = IPAT_STRLEN_SAFE;

    ret = ipat_ntop(ip, dst, dstlen, cidr);
    if(!ret) {
        free(dst);
    }

    return ret;
}

bool ipat_set(ipat_t* ip, int family, const void* src, int srclen) {
    int maxsize = 0;

    if(!ip || !src) {
        errno = EINVAL;
        return false;
    }

    ip->valid = false;

    if(!ipat_supported_family(family, false)) {
        return false;
    }

    maxsize = ipat_max_size(family);
    if((maxsize == -1) || (maxsize != srclen)) {
        errno = EINVAL;
        return false;
    }

    ip->addr.any = false;
    memcpy(ip->addr.buffer, src, maxsize);
    ip->addr.family = family;
    ip->addr.prefixlen = ipat_max_prefix_length(family);
    ip->valid = true;

    return true;
}

bool ipat_set_nul(ipat_t* ip, int family) {
    if(!ip) {
        errno = EINVAL;
        return false;
    }

    ip->valid = false;

    if(!ipat_supported_family(family, false)) {
        return false;
    }

    ip->addr.any = false;
    memset(ip->addr.buffer, 0, ipat_max_size(family));
    ip->addr.family = family;
    ip->addr.prefixlen = ipat_max_prefix_length(family);
    ip->valid = true;

    return true;
}

bool ipat_set_any(ipat_t* ip, int family) {
    if(!ip) {
        errno = EINVAL;
        return false;
    }

    ip->valid = false;

    if(!ipat_supported_family(family, true)) {
        return false;
    }

    ip->addr.any = true;
    ip->addr.family = family;
    ip->valid = true;

    return true;
}

bool ipat_is_any(const ipat_t* ip) {
    if(!ip || !ip->valid) {
        errno = EINVAL;
        return false;
    }

    return ip->addr.any;
}

int ipat_family(const ipat_t* ip) {
    if(!ip || !ip->valid) {
        errno = EINVAL;
        return AF_INET_INVALID;
    }

    return ip->addr.family;
}

void* ipat_buffer(const ipat_t* ip, int* size) {
    int maxsize = 0;
    unsigned char* buffer = NULL;

    if(!ip || !ip->valid || ip->addr.any) {
        errno = EINVAL;
        return NULL;
    }

    maxsize = ipat_max_size(ip->addr.family);
    if(maxsize == -1) {
        errno = EINVAL;
        return NULL;
    }

    buffer = calloc(1, maxsize);
    if(!buffer) {
        errno = ENOMEM;
        return NULL;
    }

    memcpy(buffer, ip->addr.buffer, maxsize);
    if(size) {
        *size = maxsize;
    }

    return buffer;
}

void* ipat_da_buffer(ipat_t* ip, int* size) {
    int maxsize = 0;

    if(!ip || !ip->valid || ip->addr.any) {
        errno = EINVAL;
        return NULL;
    }

    maxsize = ipat_max_size(ip->addr.family);
    if(maxsize == -1) {
        errno = EINVAL;
        return NULL;
    }

    if(size) {
        *size = (int) maxsize;
    }

    return (void*) ip->addr.buffer;
}

int ipat_prefix_length(const ipat_t* ip) {
    if(!ip || !ip->valid || ip->addr.any) {
        errno = EINVAL;
        return -1;
    }

    return ip->addr.prefixlen;
}

bool ipat_set_prefix_length(ipat_t* ip, int prefixlen) {
    int maxprefixlength = 0;

    if(!ip || !ip->valid || ip->addr.any) {
        errno = EINVAL;
        return false;
    }

    maxprefixlength = ipat_max_prefix_length(ip->addr.family);
    if((maxprefixlength == -1) || (prefixlen < 0) || (prefixlen > maxprefixlength)) {
        errno = EINVAL;
        return false;
    }

    ip->addr.prefixlen = prefixlen;

    return true;
}

unsigned int ipat_type(const ipat_t* ip, unsigned int* family) {
    unsigned int type = IPAT_TYPE_NONE, type_family = IPAT_TYPE_NONE;

    if(!ip || !ip->valid) {
        errno = EINVAL;
        return IPAT_TYPE_INVALID;
    }

    if(!ipat_type_common_pre(ip, &type, &type_family)) {
        return IPAT_TYPE_INVALID;
    }

    switch(ip->addr.family) {
    case AF_INET_ANY:
        if(!ipat_type_ipvany(ip, &type, &type_family)) {
            return IPAT_TYPE_INVALID;
        }
        break;
    case AF_INET:
        if(!ipat_type_ipv4(ip, &type, &type_family)) {
            return IPAT_TYPE_INVALID;
        }
        break;
    case AF_INET6:
        if(!ipat_type_ipv6(ip, &type, &type_family)) {
            return IPAT_TYPE_INVALID;
        }
        break;
    default:
        errno = EAFNOSUPPORT;
        return IPAT_TYPE_INVALID;
        break;
    }

    if(!ipat_type_common_post(ip, &type, &type_family)) {
        return IPAT_TYPE_INVALID;
    }

    if(family) {
        *family = type_family;
    }

    return type;
}

bool ipat_mask_create(ipat_t* ip, int family, int len, ipat_bits_t bits) {
    if(!ip) {
        errno = EINVAL;
        return false;
    }

    ip->valid = ipat_address_mask_create(&(ip->addr), family, len, bits);

    return ip->valid;
}

bool ipat_mask_apply(ipat_t* ip, const ipat_t* mask, ipat_oper_t oper) {
    if(!ip || !ip->valid || !mask || !mask->valid) {
        errno = EINVAL;
        return false;
    }

    return ipat_address_mask_apply(&(ip->addr), &(mask->addr), oper);
}

bool ipat_mask_apply_direct(ipat_t* ip, int len, ipat_bits_t bits, ipat_oper_t oper) {
    int maxsize = 0;
    unsigned char mask[IPAT_SIZE_SAFE];

    if(!ip || !ip->valid || ip->addr.any) {
        errno = EINVAL;
        return false;
    }

    maxsize = ipat_max_size(ip->addr.family);
    if(maxsize == -1) {
        errno = EINVAL;
        return false;
    }

    if(ipat_buffer_mask_create(mask, maxsize, len, bits)) {
        return ipat_buffer_mask_apply(ip->addr.buffer, mask, maxsize, oper);
    }

    return false;
}

bool ipat_to_subnet(ipat_t* subnet, const ipat_t* ip) {
    if(!ipat_copy(subnet, ip)) {
        return false;
    }

    if(!ipat_mask_apply_direct(subnet, ipat_prefix_length(subnet), ipat_bits_upper, ipat_oper_and)) {
        return false;
    }

    return true;
}

int ipat_compare(const ipat_t* a, const ipat_t* b, bool prefixlen) {
    if(!a || !a->valid || !b || !b->valid) {
        errno = EINVAL;
        return IPAT_COMPARE_ERROR;
    }

    return ipat_address_compare(&(a->addr), &(b->addr), prefixlen);
}

bool ipat_in_subnet(const ipat_t* ip, const ipat_t* subnet) {
    if(!ip || !ip->valid || !subnet || !subnet->valid) {
        errno = EINVAL;
        return false;
    }

    return ipat_address_in_subnet(&(ip->addr), &(subnet->addr));
}

bool ipat_make_EUI64(ipat_t* ip, const ipat_mac_t* mac) {
    unsigned char eui64[8];

    if(!ip || !ip->valid || (ip->addr.family != AF_INET6) || ip->addr.any || !mac || !mac->valid) {
        errno = EINVAL;
        return false;
    }

    if(!ipat_buffer_fill_EUI64_bin(eui64, sizeof(eui64), mac->buffer, IPAT_SIZE_MAC)) {
        return false;
    }

    memcpy(&(ip->addr.buffer[8]), eui64, 8);

    return true;
}

bool ipat_is_EUI64(const ipat_t* ip, const ipat_mac_t* mac) {
    unsigned char eui64[8];

    if(!ip || !ip->valid || (ip->addr.family != AF_INET6) || ip->addr.any || !mac || !mac->valid) {
        errno = EINVAL;
        return false;
    }

    if(!ipat_buffer_fill_EUI64_bin(eui64, sizeof(eui64), mac->buffer, IPAT_SIZE_MAC)) {
        return false;
    }

    return !memcmp(&(ip->addr.buffer[8]), eui64, 8);
}

bool ipat_make_EUI64_text(ipat_t* ip, const char* mac) {
    unsigned char eui64[8];

    if(!ip || !ip->valid || (ip->addr.family != AF_INET6) || ip->addr.any || !mac) {
        errno = EINVAL;
        return false;
    }

    if(!ipat_buffer_fill_EUI64(eui64, sizeof(eui64), mac)) {
        return false;
    }

    memcpy(&(ip->addr.buffer[8]), eui64, 8);

    return true;
}

bool ipat_is_EUI64_text(const ipat_t* ip, const char* mac) {
    unsigned char eui64[8];

    if(!ip || !ip->valid || (ip->addr.family != AF_INET6) || ip->addr.any || !mac) {
        errno = EINVAL;
        return false;
    }

    if(!ipat_buffer_fill_EUI64(eui64, sizeof(eui64), mac)) {
        return false;
    }

    return !memcmp(&(ip->addr.buffer[8]), eui64, 8);
}

int ipat_mask_prefix_length(int family, const char* mask) {
    return ipat_mask_prefix_length_family(family, mask, NULL);
}

bool ipat_convert(ipat_t* ip, int to_family) {
    int offset = 0;
    unsigned char buffer[IPAT_SIZE_SAFE];

    if(!ip || !ip->valid || ip->addr.any) {
        errno = EINVAL;
        return false;
    }

    if(!ipat_supported_family(to_family, false)) {
        return false;
    }

    if(ip->addr.family == to_family) {
        return true;
    }

    /*
     * IPv4 mapped addresses in IPv6 use
     * the ::ffff:0:0/96 prefix.
     * The prefix length is dividable by 8,
     * so we can use memcpy() and memcmp() directly.
     */
    inet_pton(AF_INET6, "::ffff:0:0", buffer);
    offset = 96 / 8;

    /*
     * Only 2 families supported right now, so
     * not too much checking required.
     */
    switch(ip->addr.family) {
    case AF_INET:
        memcpy(buffer + offset, ip->addr.buffer, IPAT_SIZE_IPV4);
        memcpy(ip->addr.buffer, buffer, IPAT_SIZE_IPV6);
        ip->addr.prefixlen += 96;
        ip->addr.family = AF_INET6;
        break;
    case AF_INET6:
        if(memcmp(ip->addr.buffer, buffer, offset) || (ip->addr.prefixlen < 96)) {
            errno = EINVAL;
            return false;
        }
        memcpy(ip->addr.buffer, ip->addr.buffer + offset, IPAT_SIZE_IPV4);
        ip->addr.prefixlen -= 96;
        ip->addr.family = AF_INET;
        break;
    default:
        errno = EAFNOSUPPORT;
        return false;
        break;
    }

    return true;
}

