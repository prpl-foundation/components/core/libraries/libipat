/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <ipat/ipat.h>

#include "ipat_buffer.h"

typedef struct ipat_type_address_t {
    const char* ipt;
    int prefixlen;
    unsigned int common;
    unsigned int family;
    unsigned char buffer[IPAT_SIZE_SAFE];
} ipat_type_address_t;

static bool ipat_type_address_init_ipv4 = false;
static bool ipat_type_address_init_ipv6 = false;

static ipat_type_address_t ipat_type_address_ipv4[] = {
    {
        .ipt = "0.0.0.0",
        .prefixlen = 8,
        .common = IPAT_TYPE_BROADCAST | IPAT_TYPE_SPECIAL,
        .family = IPAT_TYPE_IPV4_CURRENT,
    },
    {
        .ipt = "10.0.0.0",
        .prefixlen = 8,
        .common = IPAT_TYPE_PRIVATE,
        .family = IPAT_TYPE_IPV4_PRIVATE_24,
    },
    {
        .ipt = "100.64.0.0",
        .prefixlen = 10,
        .common = IPAT_TYPE_PRIVATE,
        .family = IPAT_TYPE_IPV4_SHARED,
    },
    {
        .ipt = "127.0.0.0",
        .prefixlen = 8,
        .common = IPAT_TYPE_LOOPBACK,
        .family = IPAT_TYPE_IPV4_LOOPBACK,
    },
    {
        .ipt = "169.254.0.0",
        .prefixlen = 16,
        .common = IPAT_TYPE_LINKLOCAL,
        .family = IPAT_TYPE_IPV4_LINKLOCAL,
    },
    {
        .ipt = "172.16.0.0",
        .prefixlen = 12,
        .common = IPAT_TYPE_PRIVATE,
        .family = IPAT_TYPE_IPV4_PRIVATE_20,
    },
    {
        .ipt = "192.0.0.0",
        .prefixlen = 24,
        .common = IPAT_TYPE_SPECIAL,
        .family = IPAT_TYPE_IPV4_SPECIAL,
    },
    {
        .ipt = "192.0.2.0",
        .prefixlen = 24,
        .common = IPAT_TYPE_DOC,
        .family = IPAT_TYPE_IPV4_DOC_1,
    },
    {
        .ipt = "192.88.99.0",
        .prefixlen = 24,
        .common = IPAT_TYPE_ANYCAST,
        .family = IPAT_TYPE_IPV4_6TO4RELAY,
    },
    {
        .ipt = "192.168.0.0",
        .prefixlen = 16,
        .common = IPAT_TYPE_PRIVATE,
        .family = IPAT_TYPE_IPV4_PRIVATE_16,
    },
    {
        .ipt = "198.18.0.0",
        .prefixlen = 15,
        .common = IPAT_TYPE_PRIVATE,
        .family = IPAT_TYPE_IPV4_BENCHMARK,
    },
    {
        .ipt = "198.51.100.0",
        .prefixlen = 24,
        .common = IPAT_TYPE_DOC,
        .family = IPAT_TYPE_IPV4_DOC_2,
    },
    {
        .ipt = "203.0.113.0",
        .prefixlen = 24,
        .common = IPAT_TYPE_DOC,
        .family = IPAT_TYPE_IPV4_DOC_3,
    },
    {
        .ipt = "224.0.0.0",
        .prefixlen = 4,
        .common = IPAT_TYPE_MULTICAST,
        .family = IPAT_TYPE_IPV4_MULTICAST,
    },
    {   /* Must be placed before 240.0.0.0/4 */
        .ipt = "255.255.255.255",
        .prefixlen = 32,
        .common = IPAT_TYPE_BROADCAST,
        .family = IPAT_TYPE_IPV4_BROADCAST,
    },
    {
        .ipt = "240.0.0.0",
        .prefixlen = 4,
        .common = IPAT_TYPE_RESERVED,
        .family = IPAT_TYPE_IPV4_RESERVED,
    },
    {
        .ipt = NULL,
    },
};

static ipat_type_address_t ipat_type_address_ipv6[] = {
    {
        .ipt = "::",
        .prefixlen = 128,
        .common = IPAT_TYPE_SPECIAL,
        .family = IPAT_TYPE_IPV6_UNSPECIFIED,
    },
    {
        .ipt = "::1",
        .prefixlen = 128,
        .common = IPAT_TYPE_LOOPBACK,
        .family = IPAT_TYPE_IPV6_LOOPBACK,
    },
    {
        .ipt = "::ffff:0:0",
        .prefixlen = 96,
        .common = IPAT_TYPE_SPECIAL,
        .family = IPAT_TYPE_IPV6_IPV4MAPPED,
    },
    {
        .ipt = "100::",
        .prefixlen = 64,
        .common = IPAT_TYPE_SPECIAL,
        .family = IPAT_TYPE_IPV6_DISCARD,
    },
    {
        .ipt = "64:ff9b::",
        .prefixlen = 96,
        .common = IPAT_TYPE_PUBLIC,
        .family = IPAT_TYPE_IPV6_4TO6TRANSL,
    },
    {
        .ipt = "2001::",
        .prefixlen = 32,
        .common = IPAT_TYPE_PUBLIC,
        .family = IPAT_TYPE_IPV6_TEREDO,
    },
    {
        .ipt = "2001:10::",
        .prefixlen = 28,
        .common = IPAT_TYPE_SPECIAL,
        .family = IPAT_TYPE_IPV6_ORCHID,
    },
    {
        .ipt = "2001:20::",
        .prefixlen = 28,
        .common = IPAT_TYPE_SPECIAL,
        .family = IPAT_TYPE_IPV6_ORCHID2,
    },
    {
        .ipt = "2001:db8::",
        .prefixlen = 32,
        .common = IPAT_TYPE_DOC,
        .family = IPAT_TYPE_IPV6_DOC,
    },
    {
        .ipt = "2002::",
        .prefixlen = 16,
        .common = IPAT_TYPE_PUBLIC,
        .family = IPAT_TYPE_IPV6_6TO4,
    },
    {
        .ipt = "fc00::",
        .prefixlen = 7,
        .common = IPAT_TYPE_PRIVATE,
        .family = IPAT_TYPE_IPV6_ULA,
    },
    {
        .ipt = "fe80::",
        .prefixlen = 10,
        .common = IPAT_TYPE_LINKLOCAL,
        .family = IPAT_TYPE_IPV6_LLA,
    },
    {
        .ipt = "ff00::",
        .prefixlen = 8,
        .common = IPAT_TYPE_MULTICAST | IPAT_TYPE_SPECIAL,
        .family = IPAT_TYPE_IPV6_MULTICAST,
    },
    {
        .ipt = NULL,
    },
};

static bool ipat_type_ipv4_broadcast(const ipat_t* ip, unsigned int* common, unsigned int* family) {
    int maxsize = 0;
    unsigned char buffer[IPAT_SIZE_SAFE], mask[IPAT_SIZE_SAFE];

    maxsize = ipat_max_size(ip->addr.family);
    if(maxsize == -1) {
        return false;
    }

    if(ip->addr.prefixlen == ipat_max_prefix_length(ip->addr.family)) {
        return true;
    }

    memcpy(buffer, ip->addr.buffer, maxsize);
    ipat_buffer_mask_create(mask, maxsize, ip->addr.prefixlen, ipat_bits_lower);
    ipat_buffer_mask_apply(buffer, mask, maxsize, ipat_oper_and);
    if(!memcmp(buffer, mask, maxsize)) {
        *common |= IPAT_TYPE_BROADCAST;
        *family |= IPAT_TYPE_IPV4_BROADCAST;
    }

    return true;
}

bool ipat_type_common_pre(const ipat_t* ip, unsigned int* common, unsigned int* family) {
    int maxsize = 0;
    unsigned char buffer[IPAT_SIZE_SAFE], mask[IPAT_SIZE_SAFE];

    (void) family;

    if(ip->addr.any) {
        *common |= IPAT_TYPE_ANY;
        return true;
    }

    maxsize = ipat_max_size(ip->addr.family);
    if(maxsize == -1) {
        return false;
    }

    memset(mask, 0, maxsize);
    if(!memcmp(ip->addr.buffer, mask, maxsize)) {
        *common |= IPAT_TYPE_NUL;
        return true;
    }

    ipat_buffer_mask_create(mask, maxsize, ip->addr.prefixlen, ipat_bits_upper);
    if(!memcmp(ip->addr.buffer, mask, maxsize)) {
        *common |= IPAT_TYPE_MASK;
        return true;
    }

    if(ip->addr.prefixlen < ipat_max_prefix_length(ip->addr.family)) {
        memcpy(buffer, ip->addr.buffer, maxsize);
        ipat_buffer_mask_apply(buffer, mask, maxsize, ipat_oper_and);
        if(!memcmp(ip->addr.buffer, buffer, maxsize)) {
            *common |= IPAT_TYPE_SUBNET;
            return true;
        }
    }

    *common |= IPAT_TYPE_HOST;
    return true;
}

bool ipat_type_common_post(const ipat_t* ip, unsigned int* common, unsigned int* family) {
    (void) family;

    if(ip->addr.any || (*common == IPAT_TYPE_NONE)) {
        return true;
    }

    if((*common & (IPAT_TYPE_NUL | IPAT_TYPE_MASK)) != 0) {
        return true;
    }

    if((*common & IPAT_TYPE_MASK_CAST) == 0) {
        *common |= IPAT_TYPE_UNICAST;
    }

    if((*common & IPAT_TYPE_MASK_SCOPE) == 0) {
        *common |= IPAT_TYPE_PUBLIC;
    }

    return true;
}

bool ipat_type_ipvany(const ipat_t* ip, unsigned int* common, unsigned int* family) {
    (void) ip;
    (void) family;

    *common |= IPAT_TYPE_IPVANY;

    return true;
}

bool ipat_type_ipv4(const ipat_t* ip, unsigned int* common, unsigned int* family) {
    ipat_type_address_t* ipa = NULL;

    *common |= IPAT_TYPE_IPV4;

    if(ip->addr.any) {
        return true;
    }

    /* Initialize */
    if(!ipat_type_address_init_ipv4) {
        for(ipa = ipat_type_address_ipv4; ipa->ipt != NULL; ipa++) {
            inet_pton(AF_INET, ipa->ipt, ipa->buffer);
        }

        ipat_type_address_init_ipv4 = true;
    }

    /* Check subnets */
    for(ipa = ipat_type_address_ipv4; ipa->ipt != NULL; ipa++) {
        if(ipat_buffer_in_subnet(ip->addr.buffer, ipa->buffer, ipa->prefixlen)) {
            *common |= ipa->common;
            *family |= ipa->family;

            break;
        }
    }

    if((*common & (IPAT_TYPE_NUL | IPAT_TYPE_MASK)) != 0) {
        return true;
    }

    /* Extra check for broadcast */
    if(!ipat_type_ipv4_broadcast(ip, common, family)) {
        return false;
    }

    /* Custom check for public */
    if((*common & IPAT_TYPE_MASK_SCOPE) == 0) {
        *common |= IPAT_TYPE_PUBLIC;
        *family |= IPAT_TYPE_IPV4_PUBLIC;
    }

    return true;
}

bool ipat_type_ipv6(const ipat_t* ip, unsigned int* common, unsigned int* family) {
    ipat_type_address_t* ipa = NULL;

    *common |= IPAT_TYPE_IPV6;

    if(ip->addr.any) {
        return true;
    }

    /* Initialize */
    if(!ipat_type_address_init_ipv6) {
        for(ipa = ipat_type_address_ipv6; ipa->ipt != NULL; ipa++) {
            inet_pton(AF_INET6, ipa->ipt, ipa->buffer);
        }

        ipat_type_address_init_ipv6 = true;
    }

    /* Check subnets */
    for(ipa = ipat_type_address_ipv6; ipa->ipt != NULL; ipa++) {
        if(ipat_buffer_in_subnet(ip->addr.buffer, ipa->buffer, ipa->prefixlen)) {
            *common |= ipa->common;
            *family |= ipa->family;

            break;
        }
    }

    /* Custom check for mc scope */
    if((*family & IPAT_TYPE_IPV6_MULTICAST) != 0) {
        *family |= ((unsigned int) (ip->addr.buffer[1] & 0x0F)) << 28;
    }

    if((*common & (IPAT_TYPE_NUL | IPAT_TYPE_MASK)) != 0) {
        return true;
    }

    /* Custom check for GUA */
    if((*common & IPAT_TYPE_MASK_SCOPE) == 0) {
        *common |= IPAT_TYPE_PUBLIC;
        *family |= IPAT_TYPE_IPV6_GUA;
    }

    /* Custom check for modified EUI-64 */
    if((*common & IPAT_TYPE_HOST) != 0) {
        if((ip->addr.buffer[11] == 0xFF) && (ip->addr.buffer[12] == 0xFE)) {
            *family |= IPAT_TYPE_IPV6_EUI64;
        }
    }

    return true;
}

