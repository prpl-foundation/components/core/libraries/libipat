/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <ipat/ipat.h>

#define UPPER_MASK(x)   (0xFF << (8 - (x % 8)))
#define LOWER_MASK(x)   (0xFF >> (x % 8))

static char* ipat_buffer_mac_get_format(const char* mac, char* format) {
    if(strlen(mac) == 17) {
        if(!ipat_mac_valid_separator(mac[2], NULL)) {
            errno = EINVAL;
            return NULL;
        }

        strcpy(format, "%2hhxs%2hhxs%2hhxs%2hhxs%2hhxs%2hhx");

        format[5] = mac[2];
        format[11] = mac[2];
        format[17] = mac[2];
        format[23] = mac[2];
        format[29] = mac[2];
    } else if(strlen(mac) == 12) {
        strcpy(format, "%2hhx%2hhx%2hhx%2hhx%2hhx%2hhx");
    } else {
        errno = EINVAL;
        return NULL;
    }

    return format;
}

bool ipat_buffer_fill_mac(unsigned char* dst, int size, const char* mac) {
    char format[50] = "";

    if(size != 6) {
        errno = EINVAL;
        return false;
    }

    if(!ipat_buffer_mac_get_format(mac, format)) {
        return false;
    }

    errno = 0;
    if(sscanf(mac, format, &dst[0], &dst[1], &dst[2], &dst[3], &dst[4], &dst[5]) != 6) {
        if(errno == 0) {
            errno = EINVAL;
        }
        return false;
    }

    return true;
}

bool ipat_buffer_fill_EUI64(unsigned char* dst, int size, const char* mac) {
    char format[50] = "";

    if(size != 8) {
        errno = EINVAL;
        return false;
    }

    if(!ipat_buffer_mac_get_format(mac, format)) {
        return false;
    }

    errno = 0;
    if(sscanf(mac, format, &dst[0], &dst[1], &dst[2], &dst[5], &dst[6], &dst[7]) != 6) {
        if(errno == 0) {
            errno = EINVAL;
        }
        return false;
    }

    dst[0] ^= 0x02;
    dst[3] = 0xFF;
    dst[4] = 0xFE;

    return true;
}

bool ipat_buffer_fill_EUI64_bin(unsigned char* dst, int size, const void* src, int srcsize) {
    if((size != 8) || (srcsize != 6)) {
        errno = EINVAL;
        return false;
    }

    memcpy(dst, src, 3);
    memcpy(dst + 5, src + 3, 3);

    dst[0] ^= 0x02;
    dst[3] = 0xFF;
    dst[4] = 0xFE;

    return true;
}

bool ipat_buffer_mask_create(unsigned char* dst, int size, int prefixlen, ipat_bits_t bits) {
    int i;

    if(prefixlen > size * 8) {
        errno = EINVAL;
        return false;
    }

    i = prefixlen / 8;
    if(i != 0) {
        memset(dst, bits == ipat_bits_upper ? 0xFF : 0x00, i);
    }

    if((prefixlen % 8) != 0) {
        dst[i++] = bits == ipat_bits_upper ? UPPER_MASK(prefixlen) : LOWER_MASK(prefixlen);
    }

    if(i != size) {
        memset(&(dst[i]), bits == ipat_bits_upper ? 0x00 : 0xFF, size - i);
    }

    return true;
}

bool ipat_buffer_mask_apply(unsigned char* dst, const unsigned char* src, int size, ipat_oper_t oper) {
    int i, intsize = (int) sizeof(unsigned int);
    unsigned int* dst_int = (unsigned int*) dst;
    const unsigned int* src_int = (const unsigned int*) src;

    for(i = 0; i < size / intsize; i++) {
        switch(oper) {
        case ipat_oper_and:
            dst_int[i] &= src_int[i];
            break;
        case ipat_oper_or:
            dst_int[i] |= src_int[i];
            break;
        case ipat_oper_xor:
            dst_int[i] ^= src_int[i];
            break;
        default:
            errno = EINVAL;
            return false;
            break;
        }
    }

    if((size % intsize) != 0) {
        for(i *= intsize; i < size; i++) {
            switch(oper) {
            case ipat_oper_and:
                dst[i] &= src[i];
                break;
            case ipat_oper_or:
                dst[i] |= src[i];
                break;
            case ipat_oper_xor:
                dst[i] ^= src[i];
                break;
            default:
                errno = EINVAL;
                return false;
                break;
            }
        }
    }

    return true;
}

bool ipat_buffer_in_subnet(const unsigned char* ip, const unsigned char* subnet, int prefixlen) {
    int i;
    unsigned char mask = 0x00;

    i = prefixlen / 8;
    if((i != 0) && (memcmp(ip, subnet, i) != 0)) {
        return false;
    }

    if((prefixlen % 8) != 0) {
        mask = UPPER_MASK(prefixlen);

        if((ip[i] & mask) != (subnet[i] & mask)) {
            return false;
        }
    }

    return true;
}

