/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <ipat/ipat.h>

#include "ipat_buffer.h"

static int ipat_address_compare_any(const ipat_address_t* a, const ipat_address_t* b) {
    if((a->family != b->family) && (a->family != AF_INET_ANY) && (b->family != AF_INET_ANY)) {
        errno = EINVAL;
        return IPAT_COMPARE_ERROR;
    }

    return IPAT_COMPARE_EQUAL;
}

bool ipat_address_mask_create(ipat_address_t* dst, int family, int prefixlen, ipat_bits_t bits) {
    int maxsize = ipat_max_size(family);

    if((maxsize == -1) || (prefixlen > ipat_max_prefix_length(family))) {
        errno = EINVAL;
        return false;
    }

    dst->any = false;
    dst->family = family;
    dst->prefixlen = prefixlen;

    return ipat_buffer_mask_create(dst->buffer, maxsize, prefixlen, bits);
}

bool ipat_address_mask_apply(ipat_address_t* dst, const ipat_address_t* src, ipat_oper_t oper) {
    int maxsize = ipat_max_size(dst->family);

    if((maxsize == -1) || (dst->family != src->family) || dst->any || src->any) {
        errno = EINVAL;
        return false;
    }

    return ipat_buffer_mask_apply(dst->buffer, src->buffer, maxsize, oper);
}

int ipat_address_compare(const ipat_address_t* a, const ipat_address_t* b, bool prefixlen) {
    int maxsize = 0, result = IPAT_COMPARE_ERROR;

    if(a->any || b->any) {
        return ipat_address_compare_any(a, b);
    }

    if(a->family != b->family) {
        errno = EINVAL;
        return IPAT_COMPARE_ERROR;
    }

    maxsize = ipat_max_size(a->family);
    if(maxsize == -1) {
        errno = EINVAL;
        return IPAT_COMPARE_ERROR;
    }

    result = memcmp(a->buffer, b->buffer, maxsize);

    if((result == 0) && prefixlen) {
        result = a->prefixlen - b->prefixlen;
    }

    /* Normalize result */
    if(result < 0) {
        result = IPAT_COMPARE_LESS;
    } else if(result > 0) {
        result = IPAT_COMPARE_GREATER;
    } else {
        result = IPAT_COMPARE_EQUAL;
    }

    return result;
}

bool ipat_address_in_subnet(const ipat_address_t* ip, const ipat_address_t* subnet) {
    if(subnet->any) {
        errno = EINVAL;
        return false;
    }

    if(ip->any && ((ip->family == AF_INET_ANY) || (ip->family == subnet->family))) {
        return true;
    }

    if(ip->family != subnet->family) {
        errno = EINVAL;
        return false;
    }

    if(ip->prefixlen < subnet->prefixlen) {
        return false;
    }

    return ipat_buffer_in_subnet(ip->buffer, subnet->buffer, subnet->prefixlen);
}

