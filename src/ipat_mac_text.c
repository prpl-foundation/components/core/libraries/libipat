/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <ipat/ipat_mac.h>

char* ipat_mac_text_copy(const char* mact, ipat_mac_sep_t sep, bool upper) {
    ipat_mac_t src = IPAT_MAC_INITIALIZE, dst = IPAT_MAC_INITIALIZE;

    ipat_mac_pton(&src, mact);
    if(ipat_mac_copy(&dst, &src)) {
        return ipat_mac_ntop_alloc(&dst, sep, upper);
    }

    return NULL;
}

bool ipat_mac_text_valid(const char* mact) {
    ipat_mac_t mac = IPAT_MAC_INITIALIZE;

    return ipat_mac_pton(&mac, mact);
}

void* ipat_mac_text_buffer(const char* mact) {
    ipat_mac_t mac = IPAT_MAC_INITIALIZE;

    ipat_mac_pton(&mac, mact);
    return ipat_mac_buffer(&mac);
}

char* ipat_mac_text_set(void* src, int srclen, ipat_mac_sep_t sep, bool upper) {
    ipat_mac_t mac = IPAT_MAC_INITIALIZE;

    if(ipat_mac_set(&mac, src, srclen)) {
        return ipat_mac_ntop_alloc(&mac, sep, upper);
    }

    return NULL;
}

char* ipat_mac_text_set_nul(ipat_mac_sep_t sep) {
    ipat_mac_t mac = IPAT_MAC_INITIALIZE;

    if(ipat_mac_set_nul(&mac)) {
        return ipat_mac_ntop_alloc(&mac, sep, false);
    }

    return NULL;
}

bool ipat_mac_text_get_part(const char* mact, void* dst, int dstlen, ipat_mac_part_t part) {
    ipat_mac_t mac = IPAT_MAC_INITIALIZE;

    ipat_mac_pton(&mac, mact);
    return ipat_mac_get_part(&mac, dst, dstlen, part);
}

char* ipat_mac_text_set_part(const char* mact, void* src, int srclen, ipat_mac_part_t part, ipat_mac_sep_t sep, bool upper) {
    ipat_mac_t mac = IPAT_MAC_INITIALIZE;

    ipat_mac_pton(&mac, mact);
    if(ipat_mac_set_part(&mac, src, srclen, part)) {
        return ipat_mac_ntop_alloc(&mac, sep, upper);
    }

    return NULL;
}

bool ipat_mac_text_is_unicast(const char* mact) {
    ipat_mac_t mac = IPAT_MAC_INITIALIZE;

    ipat_mac_pton(&mac, mact);
    return ipat_mac_is_unicast(&mac);
}

char* ipat_mac_text_set_unicast(const char* mact, bool unicast, ipat_mac_sep_t sep, bool upper) {
    ipat_mac_t mac = IPAT_MAC_INITIALIZE;

    ipat_mac_pton(&mac, mact);
    if(ipat_mac_set_unicast(&mac, unicast)) {
        return ipat_mac_ntop_alloc(&mac, sep, upper);
    }

    return NULL;
}

bool ipat_mac_text_is_local(const char* mact) {
    ipat_mac_t mac = IPAT_MAC_INITIALIZE;

    ipat_mac_pton(&mac, mact);
    return ipat_mac_is_local(&mac);
}

char* ipat_mac_text_set_local(const char* mact, bool local, ipat_mac_sep_t sep, bool upper) {
    ipat_mac_t mac = IPAT_MAC_INITIALIZE;

    ipat_mac_pton(&mac, mact);
    if(ipat_mac_set_local(&mac, local)) {
        return ipat_mac_ntop_alloc(&mac, sep, upper);
    }

    return NULL;
}

char* ipat_mac_text_mask_create(int len, ipat_bits_t bits, ipat_mac_sep_t sep, bool upper) {
    ipat_mac_t mac = IPAT_MAC_INITIALIZE;

    ipat_mac_mask_create(&mac, len, bits);
    return ipat_mac_ntop_alloc(&mac, sep, upper);
}

char* ipat_mac_text_mask_apply(const char* mact, const char* mask, ipat_oper_t oper, ipat_mac_sep_t sep, bool upper) {
    ipat_mac_t mac = IPAT_MAC_INITIALIZE, mac_mask = IPAT_MAC_INITIALIZE;

    ipat_mac_pton(&mac, mact);
    ipat_mac_pton(&mac_mask, mask);
    if(ipat_mac_mask_apply(&mac, &mac_mask, oper)) {
        return ipat_mac_ntop_alloc(&mac, sep, upper);
    }

    return NULL;
}

char* ipat_mac_text_mask_apply_direct(const char* mact, int len, ipat_bits_t bits, ipat_oper_t oper, ipat_mac_sep_t sep, bool upper) {
    ipat_mac_t mac = IPAT_MAC_INITIALIZE;

    ipat_mac_pton(&mac, mact);
    if(ipat_mac_mask_apply_direct(&mac, len, bits, oper)) {
        return ipat_mac_ntop_alloc(&mac, sep, upper);
    }

    return NULL;
}

int ipat_mac_text_compare(const char* a, const char* b, ipat_mac_part_t part) {
    ipat_mac_t mac_a = IPAT_MAC_INITIALIZE, mac_b = IPAT_MAC_INITIALIZE;

    ipat_mac_pton(&mac_a, a);
    ipat_mac_pton(&mac_b, b);
    return ipat_mac_compare(&mac_a, &mac_b, part);
}

