/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdarg.h>

#include <ipat/ipat.h>

#include "test.h"

static unsigned int level = -1;
static int verbose = 0;

static void help(const char* prog, FILE* fout) {
    fprintf(fout, "Usage: %s [-v] [-h]\n", prog);
}

static const char* hexify(unsigned char* data, int len, char* buffer, int size) {
    bool periods = false;
    int i;

    if(len * 2 >= size) {
        periods = true;
        len = (size - 4) / 2;
    }

    if(len < 0) {
        return "<error>";
    }

    for(i = 0; i < len; i++) {
        sprintf(buffer + (i * 2), "%02X", data[i]);
    }

    i *= 2;

    if(periods) {
        buffer[i++] = '.';
        buffer[i++] = '.';
        buffer[i++] = '.';
    }

    buffer[i] = 0;

    return buffer;
}

static bool expect(bool result, unsigned int* passed, unsigned int* total, const char* file, const char* func, int line) {
    (*total)++;

    if(result) {
        (*passed)++;
    } else {
        printl("--- Failed test %u [%s()@%s:%d]\n", *total, func, file, line);
    }

    return result;
}

bool expect_bool(bool expected, bool actual, unsigned int* passed, unsigned int* total, const char* file, const char* func, int line) {
    if((expected != actual) || verbose) {
        printl("- Test: Got '%s', expected '%s' [BOOL]\n", actual ? "true" : "false", expected ? "true" : "false");
    }

    return expect((expected == actual), passed, total, file, func, line);
}

bool expect_int(int expected, int actual, unsigned int* passed, unsigned int* total, const char* file, const char* func, int line) {
    if((expected != actual) || verbose) {
        printl("- Test: Got '%d', expected '%d' [INT]\n", actual, expected);
    }

    return expect((expected == actual), passed, total, file, func, line);
}

bool expect_uint(unsigned int expected, unsigned int actual, unsigned int* passed, unsigned int* total, const char* file, const char* func, int line) {
    if((expected != actual) || verbose) {
        printl("- Test: Got '%u', expected '%u' [UINT]\n", actual, expected);
    }

    return expect((expected == actual), passed, total, file, func, line);
}

bool expect_char(const char* expected, const char* actual, unsigned int* passed, unsigned int* total, const char* file, const char* func, int line) {
    bool result = false;

    if((expected == NULL) && (actual == NULL)) {
        result = true;
    } else if((expected == NULL) || (actual == NULL)) {
        result = false;
    } else {
        result = !strcmp(expected, actual);
    }

    if(!result || verbose) {
        printl("- Test: Got '%s', expected '%s' [CHAR]\n", actual, expected);
    }

    return expect(result, passed, total, file, func, line);
}

bool expect_any(void* expected, int esize, void* actual, int asize, unsigned int* passed, unsigned int* total, const char* file, const char* func, int line) {
    bool result = false;
    char buffer1[100] = "", buffer2[100] = "";

    if(((expected == NULL) && (esize != 0)) || ((actual == NULL) && (asize != 0))) {
        printl("!!! Program error: Unexpected input !!!");
        result = false;
    }

    if((expected == NULL) && (actual == NULL)) {
        result = true;
    } else if((expected == NULL) || (actual == NULL)) {
        result = false;
    } else if(esize != asize) {
        result = false;
    } else {
        result = !memcmp(expected, actual, esize);
    }

    if(!result || verbose) {
        printl("- Test: Got '%s/%d', expected '%s/%d' [ANY]\n", actual ? hexify(actual, asize, buffer1, sizeof(buffer1)) : "null", asize,
               expected ? hexify(expected, esize, buffer2, sizeof(buffer2)) : "null", esize);
    }

    return expect(result, passed, total, file, func, line);
}

void printl(const char* fmt, ...) {
    unsigned int i;
    va_list ap;

    for(i = 0; i < level; i++) {
        printf("  ");
    }

    va_start(ap, fmt);
    vprintf(fmt, ap);
    va_end(ap);
}

void start(const char* name) {
    level++;
    printf("\n");
    printl("Starting '%s' tests.\n", name);
}

void verdict(const char* name, unsigned int passed, unsigned int total) {
    printl("Ran all '%s' tests.\n", name);
    printl("Result: Passed %u tests out of %u.\n", passed, total);
    if(total) {
        printl("Result: Passed %u%%.\n", (passed * 100) / total);
    }
    printf("\n");
    printl("Verdict: %s\n", passed == total ? "PASSED" : "FAILED");
    printf("\n");
    level--;
}

int main(int argc, char** argv) {
    unsigned int passed = 0, total = 0;
    const char* name = "IPAT library";

    if(argc == 2) {
        if(!strcmp(argv[1], "-v")) {
            verbose++;
        } else if(!strcmp(argv[1], "-h")) {
            help(argv[0], stdout);
            return 0;
        } else {
            help(argv[0], stderr);
            return 1;
        }
    } else if(argc > 2) {
        help(argv[0], stderr);
        return 1;
    }

    printf("Starting IPAT library test program.\n");

    start(name);

    test_verify(&passed, &total);
    test_basis(&passed, &total);
    test_type(&passed, &total);
    test_mac(&passed, &total);

    verdict(name, passed, total);

    return 0;
}
