/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <ipat/ipat.h>

#include "test.h"

static void test_mac_basis(unsigned int* passed, unsigned int* total) {
    unsigned int p = 0, t = 0;
    const char* name = "basis";
    ipat_mac_t mac = IPAT_MAC_INITIALIZE, * macp = &mac;
    ipat_mac_t mac2 = IPAT_MAC_INITIALIZE, * macp2 = &mac2;
    void* buffer = NULL;
    unsigned char buffer2[IPAT_SIZE_MAC_SAFE];

    start(name);

    // Check misc
    EXPECT_INT(IPAT_SIZE_MAC, ipat_mac_max_size(ipat_mac_part_full), &p, &t);
    EXPECT_INT(IPAT_SIZE_MAC_PART, ipat_mac_max_size(ipat_mac_part_oui), &p, &t);
    EXPECT_INT(IPAT_SIZE_MAC_PART, ipat_mac_max_size(ipat_mac_part_nic), &p, &t);
    EXPECT_INT(IPAT_STRLEN_MAC, ipat_mac_min_string_length(ipat_mac_sep_colon), &p, &t);
    EXPECT_INT(IPAT_STRLEN_MAC_COMPACT, ipat_mac_min_string_length(ipat_mac_sep_none), &p, &t);
    EXPECT_BOOL(true, ipat_mac_valid_separator(0, NULL), &p, &t);
    EXPECT_BOOL(true, ipat_mac_valid_separator(':', NULL), &p, &t);
    EXPECT_BOOL(true, ipat_mac_valid_separator('-', NULL), &p, &t);
    EXPECT_BOOL(true, ipat_mac_valid_separator('_', NULL), &p, &t);
    EXPECT_BOOL(false, ipat_mac_valid_separator('*', NULL), &p, &t);
    EXPECT_BOOL(false, ipat_mac_valid_separator('%', NULL), &p, &t);
    EXPECT_BOOL(false, ipat_mac_valid_separator('a', NULL), &p, &t);

    // Check init and cleanup
    EXPECT_BOOL(false, ipat_mac_valid(macp), &p, &t);
    EXPECT_BOOL(true, ipat_mac_pton(macp, "11:22:33:44:55:66"), &p, &t);
    EXPECT_BOOL(true, ipat_mac_valid(macp), &p, &t);
    ipat_mac_cleanup(macp);
    EXPECT_BOOL(false, ipat_mac_valid(macp), &p, &t);
    ipat_mac_initialize(macp);
    EXPECT_BOOL(false, ipat_mac_valid(macp), &p, &t);

    // Check pton
    EXPECT_BOOL(true, ipat_mac_pton(macp, "11:22:33:44:55:66"), &p, &t);
    EXPECT_CHAR("11:22:33:44:55:66", ipat_mac_ntop_alloc(macp, ipat_mac_sep_colon, true), &p, &t);
    EXPECT_BOOL(true, ipat_mac_pton(macp, "11:22:33:AA:CC:FF"), &p, &t);
    EXPECT_CHAR("11:22:33:AA:CC:FF", ipat_mac_ntop_alloc(macp, ipat_mac_sep_colon, true), &p, &t);
    EXPECT_BOOL(true, ipat_mac_pton(macp, "11-22-33-AA-CC-FF"), &p, &t);
    EXPECT_CHAR("11:22:33:AA:CC:FF", ipat_mac_ntop_alloc(macp, ipat_mac_sep_colon, true), &p, &t);
    EXPECT_BOOL(true, ipat_mac_pton(macp, "11_22_33_AA_CC_FF"), &p, &t);
    EXPECT_CHAR("11:22:33:AA:CC:FF", ipat_mac_ntop_alloc(macp, ipat_mac_sep_colon, true), &p, &t);
    EXPECT_BOOL(true, ipat_mac_pton(macp, "11:22:33:aa:cc:ff"), &p, &t);
    EXPECT_CHAR("11:22:33:AA:CC:FF", ipat_mac_ntop_alloc(macp, ipat_mac_sep_colon, true), &p, &t);
    EXPECT_BOOL(true, ipat_mac_pton(macp, "112233445566"), &p, &t);
    EXPECT_CHAR("11:22:33:44:55:66", ipat_mac_ntop_alloc(macp, ipat_mac_sep_colon, true), &p, &t);
    EXPECT_BOOL(true, ipat_mac_pton(macp, "112233AACCFF"), &p, &t);
    EXPECT_CHAR("11:22:33:AA:CC:FF", ipat_mac_ntop_alloc(macp, ipat_mac_sep_colon, true), &p, &t);
    EXPECT_BOOL(false, ipat_mac_pton(macp, "11:22:33:44:55"), &p, &t);
    EXPECT_BOOL(false, ipat_mac_pton(macp, "112233:44:55966"), &p, &t);
    EXPECT_BOOL(false, ipat_mac_pton(macp, "11:22:33:44:KJ:66"), &p, &t);
    EXPECT_BOOL(false, ipat_mac_pton(macp, "112233445KJ6"), &p, &t);

    // Check ntop
    ipat_mac_pton(macp, "11:22:33:AA:CC:FF");
    EXPECT_CHAR("11:22:33:AA:CC:FF", ipat_mac_ntop_alloc(macp, ipat_mac_sep_colon, true), &p, &t);
    EXPECT_CHAR("11:22:33:aa:cc:ff", ipat_mac_ntop_alloc(macp, ipat_mac_sep_colon, false), &p, &t);
    EXPECT_CHAR("11-22-33-AA-CC-FF", ipat_mac_ntop_alloc(macp, ipat_mac_sep_dash, true), &p, &t);
    EXPECT_CHAR("11-22-33-aa-cc-ff", ipat_mac_ntop_alloc(macp, ipat_mac_sep_dash, false), &p, &t);
    EXPECT_CHAR("11_22_33_AA_CC_FF", ipat_mac_ntop_alloc(macp, ipat_mac_sep_underscore, true), &p, &t);
    EXPECT_CHAR("11_22_33_aa_cc_ff", ipat_mac_ntop_alloc(macp, ipat_mac_sep_underscore, false), &p, &t);
    EXPECT_CHAR("112233AACCFF", ipat_mac_ntop_alloc(macp, 0, true), &p, &t);
    EXPECT_CHAR("112233aaccff", ipat_mac_ntop_alloc(macp, 0, false), &p, &t);

    // Check parts
    buffer = ipat_mac_buffer(macp);
    EXPECT_BOOL(false, buffer == NULL, &p, &t);
    EXPECT_BOOL(true, ipat_mac_copy(macp2, macp), &p, &t);
    ipat_mac_get_part(macp2, buffer2, sizeof(buffer2), ipat_mac_part_full);
    EXPECT_ANY(buffer, IPAT_SIZE_MAC, buffer2, IPAT_SIZE_MAC, &p, &t);
    ipat_mac_get_part(macp2, buffer2, sizeof(buffer2), ipat_mac_part_oui);
    EXPECT_ANY(buffer, IPAT_SIZE_MAC_PART, buffer2, IPAT_SIZE_MAC_PART, &p, &t);
    ipat_mac_get_part(macp2, buffer2, sizeof(buffer2), ipat_mac_part_nic);
    EXPECT_ANY(buffer + IPAT_SIZE_MAC_PART, IPAT_SIZE_MAC_PART, buffer2, IPAT_SIZE_MAC_PART, &p, &t);
    ipat_mac_cleanup(macp2);
    ipat_mac_initialize(macp2);
    EXPECT_BOOL(true, ipat_mac_set_part(macp2, buffer, IPAT_SIZE_MAC, ipat_mac_part_full), &p, &t);
    EXPECT_INT(IPAT_COMPARE_EQUAL, ipat_mac_compare(macp, macp2, ipat_mac_part_full), &p, &t);
    EXPECT_INT(IPAT_COMPARE_EQUAL, ipat_mac_compare(macp, macp2, ipat_mac_part_oui), &p, &t);
    EXPECT_INT(IPAT_COMPARE_EQUAL, ipat_mac_compare(macp, macp2, ipat_mac_part_nic), &p, &t);
    ipat_mac_cleanup(macp2);
    ipat_mac_initialize(macp2);
    EXPECT_BOOL(true, ipat_mac_set(macp2, buffer, IPAT_SIZE_MAC), &p, &t);
    EXPECT_INT(IPAT_COMPARE_EQUAL, ipat_mac_compare(macp, macp2, ipat_mac_part_full), &p, &t);
    EXPECT_INT(IPAT_COMPARE_EQUAL, ipat_mac_compare(macp, macp2, ipat_mac_part_oui), &p, &t);
    EXPECT_INT(IPAT_COMPARE_EQUAL, ipat_mac_compare(macp, macp2, ipat_mac_part_nic), &p, &t);

    // Check masks
    ipat_mac_pton(macp, "11:22:33:AA:CC:FF");
    EXPECT_BOOL(true, ipat_mac_mask_create(macp2, 24, ipat_bits_upper), &p, &t);
    EXPECT_CHAR("FF:FF:FF:00:00:00", ipat_mac_ntop_alloc(macp2, ipat_mac_sep_colon, true), &p, &t);
    EXPECT_INT(IPAT_COMPARE_LESS, ipat_mac_compare(macp, macp2, ipat_mac_part_full), &p, &t);
    EXPECT_INT(IPAT_COMPARE_LESS, ipat_mac_compare(macp, macp2, ipat_mac_part_oui), &p, &t);
    EXPECT_INT(IPAT_COMPARE_GREATER, ipat_mac_compare(macp, macp2, ipat_mac_part_nic), &p, &t);
    EXPECT_BOOL(true, ipat_mac_mask_apply(macp, macp2, ipat_oper_and), &p, &t);
    EXPECT_CHAR("11:22:33:00:00:00", ipat_mac_ntop_alloc(macp, ipat_mac_sep_colon, true), &p, &t);
    EXPECT_INT(IPAT_COMPARE_EQUAL, ipat_mac_compare(macp, macp2, ipat_mac_part_nic), &p, &t);
    EXPECT_BOOL(true, ipat_mac_mask_apply_direct(macp, 40, ipat_bits_lower, ipat_oper_or), &p, &t);
    EXPECT_CHAR("11:22:33:00:00:FF", ipat_mac_ntop_alloc(macp, ipat_mac_sep_colon, true), &p, &t);
    EXPECT_INT(IPAT_COMPARE_GREATER, ipat_mac_compare(macp, macp2, ipat_mac_part_nic), &p, &t);
    EXPECT_INT(IPAT_COMPARE_LESS, ipat_mac_compare(macp2, macp, ipat_mac_part_nic), &p, &t);

    // Check local/unicast
    ipat_mac_pton(macp, "11:22:33:AA:CC:FF");
    EXPECT_BOOL(false, ipat_mac_is_local(macp), &p, &t);
    EXPECT_BOOL(false, ipat_mac_is_unicast(macp), &p, &t);
    EXPECT_BOOL(true, ipat_mac_set_local(macp, true), &p, &t);
    EXPECT_BOOL(true, ipat_mac_is_local(macp), &p, &t);
    EXPECT_CHAR("13:22:33:AA:CC:FF", ipat_mac_ntop_alloc(macp, ipat_mac_sep_colon, true), &p, &t);
    EXPECT_BOOL(true, ipat_mac_set_local(macp, false), &p, &t);
    EXPECT_BOOL(false, ipat_mac_is_local(macp), &p, &t);
    EXPECT_CHAR("11:22:33:AA:CC:FF", ipat_mac_ntop_alloc(macp, ipat_mac_sep_colon, true), &p, &t);
    EXPECT_BOOL(true, ipat_mac_set_unicast(macp, true), &p, &t);
    EXPECT_BOOL(true, ipat_mac_is_unicast(macp), &p, &t);
    EXPECT_CHAR("10:22:33:AA:CC:FF", ipat_mac_ntop_alloc(macp, ipat_mac_sep_colon, true), &p, &t);
    EXPECT_BOOL(true, ipat_mac_set_unicast(macp, false), &p, &t);
    EXPECT_BOOL(false, ipat_mac_is_unicast(macp), &p, &t);
    EXPECT_CHAR("11:22:33:AA:CC:FF", ipat_mac_ntop_alloc(macp, ipat_mac_sep_colon, true), &p, &t);

    verdict(name, p, t);
    *passed += p;
    *total += t;
}

void test_mac(unsigned int* passed, unsigned int* total) {
    unsigned int p = 0, t = 0;
    const char* name = "MAC addresses";

    start(name);

    test_mac_basis(&p, &t);

    verdict(name, p, t);
    *passed += p;
    *total += t;
}
