/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _IPAT_TEST_H_
#define _IPAT_TEST_H_

#define EXPECT_BOOL(e, a, p, t)         expect_bool(e, a, p, t, __FILE__, __FUNCTION__, __LINE__)
#define EXPECT_INT(e, a, p, t)          expect_int(e, a, p, t, __FILE__, __FUNCTION__, __LINE__)
#define EXPECT_UINT(e, a, p, t)         expect_uint(e, a, p, t, __FILE__, __FUNCTION__, __LINE__)
#define EXPECT_CHAR(e, a, p, t)         expect_char(e, a, p, t, __FILE__, __FUNCTION__, __LINE__)
#define EXPECT_ANY(e, es, a, as, p, t)  expect_any(e, es, a, as, p, t, __FILE__, __FUNCTION__, __LINE__)

/* Common */
bool expect_bool(bool expected, bool actual, unsigned int* passed, unsigned int* total, const char* file, const char* func, int line);
bool expect_int(int expected, int actual, unsigned int* passed, unsigned int* total, const char* file, const char* func, int line);
bool expect_uint(unsigned int expected, unsigned int actual, unsigned int* passed, unsigned int* total, const char* file, const char* func, int line);
bool expect_char(const char* expected, const char* actual, unsigned int* passed, unsigned int* total, const char* file, const char* func, int line);
bool expect_any(void* expected, int esize, void* actual, int asize, unsigned int* passed, unsigned int* total, const char* file, const char* func, int line);
void printl(const char* fmt, ...);
void start(const char* name);
void verdict(const char* name, unsigned int passed, unsigned int total);

/* Tests */
void test_verify(unsigned int* passed, unsigned int* total);
void test_basis(unsigned int* passed, unsigned int* total);
void test_type(unsigned int* passed, unsigned int* total);
void test_mac(unsigned int* passed, unsigned int* total);

#endif
