/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <ipat/ipat.h>

#include "test.h"

static void print_family_ipvany(unsigned int type) {
    if(type == IPAT_TYPE_NONE) {
        printf(" none\n");
        return;
    }

    printf("\n");
}

static void print_family_ipv6(unsigned int type) {
    if(type == IPAT_TYPE_NONE) {
        printf(" <none>\n");
        return;
    }

    if(type & IPAT_TYPE_IPV6_UNSPECIFIED) {
        printf(" unspecified");
    }
    if(type & IPAT_TYPE_IPV6_LOOPBACK) {
        printf(" loopback");
    }
    if(type & IPAT_TYPE_IPV6_IPV4MAPPED) {
        printf(" ipv4mapped");
    }
    if(type & IPAT_TYPE_IPV6_DISCARD) {
        printf(" discard");
    }
    if(type & IPAT_TYPE_IPV6_4TO6TRANSL) {
        printf(" 4to6translation");
    }
    if(type & IPAT_TYPE_IPV6_TEREDO) {
        printf(" teredo");
    }
    if(type & IPAT_TYPE_IPV6_ORCHID) {
        printf(" orchid");
    }
    if(type & IPAT_TYPE_IPV6_ORCHID2) {
        printf(" orchidv2");
    }
    if(type & IPAT_TYPE_IPV6_DOC) {
        printf(" doc");
    }
    if(type & IPAT_TYPE_IPV6_6TO4) {
        printf(" 6to4");
    }
    if(type & IPAT_TYPE_IPV6_ULA) {
        printf(" ula");
    }
    if(type & IPAT_TYPE_IPV6_LLA) {
        printf(" lla");
    }
    if(type & IPAT_TYPE_IPV6_MULTICAST) {
        printf(" multicast scope:");
        switch(type & IPAT_TYPE_IPV6_MC_SCOPE) {
        case IPAT_TYPE_IPV6_MCSCOPE_RESERVED0:
            printf("reserved0");
            break;
        case IPAT_TYPE_IPV6_MCSCOPE_INTERFACE:
            printf("interface");
            break;
        case IPAT_TYPE_IPV6_MCSCOPE_LINK:
            printf("link");
            break;
        case IPAT_TYPE_IPV6_MCSCOPE_REALM:
            printf("realm");
            break;
        case IPAT_TYPE_IPV6_MCSCOPE_ADMIN:
            printf("admin");
            break;
        case IPAT_TYPE_IPV6_MCSCOPE_SITE:
            printf("site");
            break;
        case IPAT_TYPE_IPV6_MCSCOPE_ORGANIZATION:
            printf("organization");
            break;
        case IPAT_TYPE_IPV6_MCSCOPE_GLOBAL:
            printf("global");
            break;
        case IPAT_TYPE_IPV6_MCSCOPE_RESERVEDF:
            printf("reservedF");
            break;
        default:
            printf("unknown");
        }
    }
    if(type & IPAT_TYPE_IPV6_GUA) {
        printf(" gua");
    }
    if(type & IPAT_TYPE_IPV6_EUI64) {
        printf(" eui64");
    }

    printf("\n");
}

static void print_family_ipv4(unsigned int type) {
    if(type == IPAT_TYPE_NONE) {
        printf(" <none>\n");
        return;
    }

    if(type & IPAT_TYPE_IPV4_CURRENT) {
        printf(" current");
    }
    if(type & IPAT_TYPE_IPV4_PRIVATE_24) {
        printf(" private24");
    }
    if(type & IPAT_TYPE_IPV4_SHARED) {
        printf(" shared");
    }
    if(type & IPAT_TYPE_IPV4_LOOPBACK) {
        printf(" loopback");
    }
    if(type & IPAT_TYPE_IPV4_LINKLOCAL) {
        printf(" linklocal");
    }
    if(type & IPAT_TYPE_IPV4_PRIVATE_20) {
        printf(" private20");
    }
    if(type & IPAT_TYPE_IPV4_SPECIAL) {
        printf(" special");
    }
    if(type & IPAT_TYPE_IPV4_DOC_1) {
        printf(" doc1");
    }
    if(type & IPAT_TYPE_IPV4_6TO4RELAY) {
        printf(" 6to4relay");
    }
    if(type & IPAT_TYPE_IPV4_PRIVATE_16) {
        printf(" private16");
    }
    if(type & IPAT_TYPE_IPV4_BENCHMARK) {
        printf(" benchmark");
    }
    if(type & IPAT_TYPE_IPV4_DOC_2) {
        printf(" doc2");
    }
    if(type & IPAT_TYPE_IPV4_DOC_3) {
        printf(" doc3");
    }
    if(type & IPAT_TYPE_IPV4_MULTICAST) {
        printf(" multicast");
    }
    if(type & IPAT_TYPE_IPV4_RESERVED) {
        printf(" reserved");
    }
    if(type & IPAT_TYPE_IPV4_BROADCAST) {
        printf(" broadcast");
    }
    if(type & IPAT_TYPE_IPV4_PUBLIC) {
        printf(" public");
    }

    printf("\n");
}

static void print_common(unsigned int type) {
    if(type == IPAT_TYPE_NONE) {
        printf(" <none>\n");
        return;
    }

    if(type & IPAT_TYPE_INVALID) {
        printf(" <invalid>");
    }
    if(type & IPAT_TYPE_IPVANY) {
        printf(" ipvany");
    }
    if(type & IPAT_TYPE_IPV4) {
        printf(" ipv4");
    }
    if(type & IPAT_TYPE_IPV6) {
        printf(" ipv6");
    }
    if(type & IPAT_TYPE_ANY) {
        printf(" any");
    }
    if(type & IPAT_TYPE_RESERVED) {
        printf(" reserved");
    }
    if(type & IPAT_TYPE_DOC) {
        printf(" doc");
    }
    if(type & IPAT_TYPE_NUL) {
        printf(" nul");
    }
    if(type & IPAT_TYPE_UNICAST) {
        printf(" unicast");
    }
    if(type & IPAT_TYPE_ANYCAST) {
        printf(" anycast");
    }
    if(type & IPAT_TYPE_MULTICAST) {
        printf(" multicast");
    }
    if(type & IPAT_TYPE_BROADCAST) {
        printf(" broadcast");
    }
    if(type & IPAT_TYPE_HOST) {
        printf(" host");
    }
    if(type & IPAT_TYPE_SUBNET) {
        printf(" subnet");
    }
    if(type & IPAT_TYPE_MASK) {
        printf(" mask");
    }
    if(type & IPAT_TYPE_LOOPBACK) {
        printf(" loopback");
    }
    if(type & IPAT_TYPE_PUBLIC) {
        printf(" public");
    }
    if(type & IPAT_TYPE_PRIVATE) {
        printf(" private");
    }
    if(type & IPAT_TYPE_LINKLOCAL) {
        printf(" linklocal");
    }
    if(type & IPAT_TYPE_SPECIAL) {
        printf(" special");
    }

    printf("\n");
}

static void print_type(const char* ip, unsigned int common, unsigned int family) {
    printl("Type '%s':\n", ip);
    printl("  common:");
    print_common(common);
    printl("  family:");
    if(common & IPAT_TYPE_INVALID) {
        printf(" <invalid>\n");
    } else if(common & IPAT_TYPE_IPVANY) {
        print_family_ipvany(family);
    } else if(common & IPAT_TYPE_IPV4) {
        print_family_ipv4(family);
    } else if(common & IPAT_TYPE_IPV6) {
        print_family_ipv6(family);
    } else {
        printf(" <unknown>\n");
    }
}

static void test_any(unsigned int* passed, unsigned int* total) {
    unsigned int p = 0, t = 0;
    const char* name = "any addresses";
    unsigned int common = IPAT_TYPE_NONE, family = IPAT_TYPE_NONE;
    ipat_t ip = IPAT_INITIALIZE, * ipp = &ip;

    start(name);

    ipat_pton(ipp, AF_INET, ipat_any4);
    common = ipat_type(ipp, &family);
    print_type(ipat_any4, common, family);
    EXPECT_UINT(IPAT_TYPE_IPV4 | IPAT_TYPE_ANY, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_NONE, family, &p, &t);

    ipat_pton(ipp, AF_INET, ipat_any);
    common = ipat_type(ipp, &family);
    print_type(ipat_any4, common, family);
    EXPECT_UINT(IPAT_TYPE_IPV4 | IPAT_TYPE_ANY, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_NONE, family, &p, &t);

    ipat_pton(ipp, AF_INET6, ipat_any6);
    common = ipat_type(ipp, &family);
    print_type(ipat_any6, common, family);
    EXPECT_UINT(IPAT_TYPE_IPV6 | IPAT_TYPE_ANY, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_NONE, family, &p, &t);

    ipat_pton(ipp, AF_INET6, ipat_any);
    common = ipat_type(ipp, &family);
    print_type(ipat_any6, common, family);
    EXPECT_UINT(IPAT_TYPE_IPV6 | IPAT_TYPE_ANY, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_NONE, family, &p, &t);

    ipat_pton(ipp, AF_INET_ANY, ipat_any4);
    common = ipat_type(ipp, &family);
    print_type(ipat_any4, common, family);
    EXPECT_UINT(IPAT_TYPE_IPV4 | IPAT_TYPE_ANY, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_NONE, family, &p, &t);

    ipat_pton(ipp, AF_INET_ANY, ipat_any6);
    common = ipat_type(ipp, &family);
    print_type(ipat_any6, common, family);
    EXPECT_UINT(IPAT_TYPE_IPV6 | IPAT_TYPE_ANY, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_NONE, family, &p, &t);

    ipat_pton(ipp, AF_INET_ANY, ipat_any);
    common = ipat_type(ipp, &family);
    print_type(ipat_any, common, family);
    EXPECT_UINT(IPAT_TYPE_IPVANY | IPAT_TYPE_ANY, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_NONE, family, &p, &t);

    verdict(name, p, t);
    *passed += p;
    *total += t;
}

static void test_ipv6(unsigned int* passed, unsigned int* total) {
    unsigned int p = 0, t = 0;
    const char* name = "IPv6 addresses";
    unsigned int common = IPAT_TYPE_NONE, family = IPAT_TYPE_NONE;
    ipat_t ip = IPAT_INITIALIZE, * ipp = &ip;

    start(name);

    ipat_pton(ipp, AF_INET6, "ffff:ffff::/32");
    common = ipat_type(ipp, &family);
    print_type("ffff:ffff::/32", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV6 | IPAT_TYPE_MASK | IPAT_TYPE_MULTICAST | IPAT_TYPE_SPECIAL, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV6_MULTICAST | IPAT_TYPE_IPV6_MCSCOPE_RESERVEDF, family, &p, &t);

    ipat_pton(ipp, AF_INET6, "::");
    common = ipat_type(ipp, &family);
    print_type("::", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV6 | IPAT_TYPE_NUL | IPAT_TYPE_SPECIAL, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV6_UNSPECIFIED, family, &p, &t);

    ipat_pton(ipp, AF_INET6, "::1");
    common = ipat_type(ipp, &family);
    print_type("::1", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV6 | IPAT_TYPE_HOST | IPAT_TYPE_UNICAST | IPAT_TYPE_LOOPBACK, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV6_LOOPBACK, family, &p, &t);

    ipat_pton(ipp, AF_INET6, "100::/64");
    common = ipat_type(ipp, &family);
    print_type("100::/64", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV6 | IPAT_TYPE_SUBNET | IPAT_TYPE_UNICAST | IPAT_TYPE_SPECIAL, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV6_DISCARD, family, &p, &t);

    ipat_pton(ipp, AF_INET6, "2001:db8::3682:20");
    common = ipat_type(ipp, &family);
    print_type("2001:db8::3682:20", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV6 | IPAT_TYPE_HOST | IPAT_TYPE_UNICAST | IPAT_TYPE_DOC, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV6_DOC, family, &p, &t);

    ipat_pton(ipp, AF_INET6, "fd00::baba:1");
    common = ipat_type(ipp, &family);
    print_type("fd00::baba:1", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV6 | IPAT_TYPE_HOST | IPAT_TYPE_UNICAST | IPAT_TYPE_PRIVATE, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV6_ULA, family, &p, &t);

    ipat_pton(ipp, AF_INET6, "fe80::2247:47ff:fe73:e147/64");
    common = ipat_type(ipp, &family);
    print_type("fe80::2247:47ff:fe73:e147/64", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV6 | IPAT_TYPE_HOST | IPAT_TYPE_UNICAST | IPAT_TYPE_LINKLOCAL, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV6_LLA | IPAT_TYPE_IPV6_EUI64, family, &p, &t);

    ipat_pton(ipp, AF_INET6, "ff03::1");
    common = ipat_type(ipp, &family);
    print_type("ff03::1", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV6 | IPAT_TYPE_HOST | IPAT_TYPE_MULTICAST | IPAT_TYPE_SPECIAL, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV6_MULTICAST | IPAT_TYPE_IPV6_MCSCOPE_REALM, family, &p, &t);

    ipat_pton(ipp, AF_INET6, "2001:470:d39b:1f80:514f:9c32:e803:dab6");
    common = ipat_type(ipp, &family);
    print_type("2001:470:d39b:1f80:514f:9c32:e803:dab6", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV6 | IPAT_TYPE_HOST | IPAT_TYPE_UNICAST | IPAT_TYPE_PUBLIC, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV6_GUA, family, &p, &t);

    verdict(name, p, t);
    *passed += p;
    *total += t;
}

static void test_ipv4(unsigned int* passed, unsigned int* total) {
    unsigned int p = 0, t = 0;
    const char* name = "IPv4 addresses";
    unsigned int common = IPAT_TYPE_NONE, family = IPAT_TYPE_NONE;
    ipat_t ip = IPAT_INITIALIZE, * ipp = &ip;

    start(name);

    ipat_pton(ipp, AF_INET, "0.0.0.0");
    common = ipat_type(ipp, &family);
    print_type("0.0.0.0", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV4 | IPAT_TYPE_NUL | IPAT_TYPE_BROADCAST | IPAT_TYPE_SPECIAL, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV4_CURRENT, family, &p, &t);

    ipat_pton(ipp, AF_INET, "255.255.0.0/16");
    common = ipat_type(ipp, &family);
    print_type("255.255.0.0/16", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV4 | IPAT_TYPE_MASK | IPAT_TYPE_RESERVED, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV4_RESERVED, family, &p, &t);

    ipat_pton(ipp, AF_INET, "255.255.255.255");
    common = ipat_type(ipp, &family);
    print_type("255.255.255.255", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV4 | IPAT_TYPE_MASK | IPAT_TYPE_BROADCAST, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV4_BROADCAST, family, &p, &t);

    ipat_pton(ipp, AF_INET, "10.3.0.0/16");
    common = ipat_type(ipp, &family);
    print_type("10.3.0.0/16", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV4 | IPAT_TYPE_SUBNET | IPAT_TYPE_UNICAST | IPAT_TYPE_PRIVATE, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV4_PRIVATE_24, family, &p, &t);

    ipat_pton(ipp, AF_INET, "100.64.4.255/24");
    common = ipat_type(ipp, &family);
    print_type("100.64.4.255/24", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV4 | IPAT_TYPE_HOST | IPAT_TYPE_BROADCAST | IPAT_TYPE_PRIVATE, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV4_SHARED | IPAT_TYPE_IPV4_BROADCAST, family, &p, &t);

    ipat_pton(ipp, AF_INET, "127.0.0.1");
    common = ipat_type(ipp, &family);
    print_type("127.0.0.1", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV4 | IPAT_TYPE_HOST | IPAT_TYPE_UNICAST | IPAT_TYPE_LOOPBACK, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV4_LOOPBACK, family, &p, &t);

    ipat_pton(ipp, AF_INET, "169.254.100.200");
    common = ipat_type(ipp, &family);
    print_type("169.254.100.200", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV4 | IPAT_TYPE_HOST | IPAT_TYPE_UNICAST | IPAT_TYPE_LINKLOCAL, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV4_LINKLOCAL, family, &p, &t);

    ipat_pton(ipp, AF_INET, "172.18.60.10");
    common = ipat_type(ipp, &family);
    print_type("172.18.60.10", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV4 | IPAT_TYPE_HOST | IPAT_TYPE_UNICAST | IPAT_TYPE_PRIVATE, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV4_PRIVATE_20, family, &p, &t);

    ipat_pton(ipp, AF_INET, "192.168.1.255/24");
    common = ipat_type(ipp, &family);
    print_type("192.168.1.255/24", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV4 | IPAT_TYPE_HOST | IPAT_TYPE_BROADCAST | IPAT_TYPE_PRIVATE, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV4_PRIVATE_16 | IPAT_TYPE_IPV4_BROADCAST, family, &p, &t);

    ipat_pton(ipp, AF_INET, "224.0.1.0/24");
    common = ipat_type(ipp, &family);
    print_type("224.0.1.0/24", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV4 | IPAT_TYPE_SUBNET | IPAT_TYPE_MULTICAST | IPAT_TYPE_PUBLIC, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV4_MULTICAST | IPAT_TYPE_IPV4_PUBLIC, family, &p, &t);

    ipat_pton(ipp, AF_INET, "123.123.123.123/17");
    common = ipat_type(ipp, &family);
    print_type("123.123.123.123/17", common, family);
    EXPECT_UINT(IPAT_TYPE_IPV4 | IPAT_TYPE_HOST | IPAT_TYPE_UNICAST | IPAT_TYPE_PUBLIC, common, &p, &t);
    EXPECT_UINT(IPAT_TYPE_IPV4_PUBLIC, family, &p, &t);

    verdict(name, p, t);
    *passed += p;
    *total += t;
}

void test_type(unsigned int* passed, unsigned int* total) {
    unsigned int p = 0, t = 0;
    const char* name = "type";

    start(name);

    test_ipv4(&p, &t);
    test_ipv6(&p, &t);
    test_any(&p, &t);

    verdict(name, p, t);
    *passed += p;
    *total += t;
}
