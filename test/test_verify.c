/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "test.h"

static void test_negative(unsigned int* passed, unsigned int* total) {
    unsigned int p = 0, t = 0;
    const char* name = "negative";
    const char* test_string = "test string", * test_string2 = "test string 2";
    const char* test_string3 = "test strong";

    start(name);

    printl("NOTICE: Following tests are expected to fail.\n");
    printl("NOTICE: The total passed tests will be inverted afterwards.\n");
    printl("NOTICE: Some 'Program error' messages are to be expected.\n");

    EXPECT_BOOL(true, false, &p, &t);
    EXPECT_BOOL(false, true, &p, &t);
    EXPECT_INT(0, 3, &p, &t);
    EXPECT_INT(2, 3, &p, &t);
    EXPECT_INT(-1, 3, &p, &t);
    EXPECT_INT(7, 3, &p, &t);
    EXPECT_INT(7, 0, &p, &t);
    EXPECT_INT(7, -5, &p, &t);
    EXPECT_INT(-3, -8, &p, &t);
    EXPECT_UINT(0, 3, &p, &t);
    EXPECT_UINT(2, 3, &p, &t);
    EXPECT_UINT(2, 0, &p, &t);
    EXPECT_UINT(9, 3, &p, &t);
    EXPECT_CHAR(test_string, NULL, &p, &t);
    EXPECT_CHAR(NULL, test_string, &p, &t);
    EXPECT_CHAR(test_string, test_string2, &p, &t);
    EXPECT_ANY(NULL, 3, (void*) test_string, strlen(test_string) + 1, &p, &t);
    EXPECT_ANY((void*) test_string, strlen(test_string) + 1, NULL, 5, &p, &t);
    EXPECT_ANY(NULL, 0, (void*) test_string, strlen(test_string) + 1, &p, &t);
    EXPECT_ANY((void*) test_string, strlen(test_string) + 1, NULL, 0, &p, &t);
    EXPECT_ANY((void*) test_string, strlen(test_string) + 1, (void*) test_string2, strlen(test_string2) + 1, &p, &t);
    EXPECT_ANY((void*) test_string, strlen(test_string) + 1, (void*) test_string3, strlen(test_string3) + 1, &p, &t);

    printl("NOTICE: Inverting %u passed tests into %u passed tests.\n", p, t - p);
    p = t - p;

    verdict(name, p, t);
    *passed += p;
    *total += t;
}

static void test_positive(unsigned int* passed, unsigned int* total) {
    unsigned int p = 0, t = 0;
    const char* name = "positive";
    const char* test_string = "test string", * test_string2 = "test string";

    start(name);

    EXPECT_BOOL(true, true, &p, &t);
    EXPECT_BOOL(false, false, &p, &t);
    EXPECT_INT(-3, -3, &p, &t);
    EXPECT_INT(0, 0, &p, &t);
    EXPECT_INT(5, 5, &p, &t);
    EXPECT_UINT(0, 0, &p, &t);
    EXPECT_UINT(7, 7, &p, &t);
    EXPECT_CHAR(NULL, NULL, &p, &t);
    EXPECT_CHAR(test_string, test_string2, &p, &t);
    EXPECT_ANY(NULL, 0, NULL, 0, &p, &t);
    EXPECT_ANY((void*) test_string, strlen(test_string) + 1, (void*) test_string2, strlen(test_string2) + 1, &p, &t);

    verdict(name, p, t);
    *passed += p;
    *total += t;
}

void test_verify(unsigned int* passed, unsigned int* total) {
    unsigned int p = 0, t = 0;
    const char* name = "verify";

    start(name);

    test_positive(&p, &t);
    test_negative(&p, &t);

    verdict(name, p, t);
    *passed += p;
    *total += t;
}
