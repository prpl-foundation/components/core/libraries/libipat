/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <ipat/ipat.h>

#include "test.h"

static void test_any(unsigned int* passed, unsigned int* total) {
    unsigned int p = 0, t = 0;
    const char* name = "any addresses";
    ipat_t ip = IPAT_INITIALIZE, * ipp = &ip;

    start(name);

    /* any4 */
    ipat_set_any(ipp, AF_INET);
    EXPECT_BOOL(true, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET, ipat_family(ipp), &p, &t);
    EXPECT_CHAR(ipat_any4, ipat_ntop_alloc(ipp, false), &p, &t);

    ipat_pton(ipp, AF_INET, ipat_any4);
    EXPECT_BOOL(true, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET, ipat_family(ipp), &p, &t);

    ipat_pton(ipp, AF_INET6, ipat_any4);
    EXPECT_BOOL(false, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET_INVALID, ipat_family(ipp), &p, &t);

    ipat_pton(ipp, AF_INET_ANY, ipat_any4);
    EXPECT_BOOL(true, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET, ipat_family(ipp), &p, &t);

    /* any6 */
    ipat_set_any(ipp, AF_INET6);
    EXPECT_BOOL(true, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET6, ipat_family(ipp), &p, &t);
    EXPECT_CHAR(ipat_any6, ipat_ntop_alloc(ipp, false), &p, &t);

    ipat_pton(ipp, AF_INET, ipat_any6);
    EXPECT_BOOL(false, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET_INVALID, ipat_family(ipp), &p, &t);

    ipat_pton(ipp, AF_INET6, ipat_any6);
    EXPECT_BOOL(true, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET6, ipat_family(ipp), &p, &t);

    ipat_pton(ipp, AF_INET_ANY, ipat_any6);
    EXPECT_BOOL(true, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET6, ipat_family(ipp), &p, &t);

    /* any */
    ipat_set_any(ipp, AF_INET_ANY);
    EXPECT_BOOL(true, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET_ANY, ipat_family(ipp), &p, &t);
    EXPECT_CHAR(ipat_any, ipat_ntop_alloc(ipp, false), &p, &t);

    ipat_pton(ipp, AF_INET, ipat_any);
    EXPECT_BOOL(true, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET, ipat_family(ipp), &p, &t);

    ipat_pton(ipp, AF_INET6, ipat_any);
    EXPECT_BOOL(true, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET6, ipat_family(ipp), &p, &t);

    ipat_pton(ipp, AF_INET_ANY, ipat_any);
    EXPECT_BOOL(true, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET_ANY, ipat_family(ipp), &p, &t);

    verdict(name, p, t);
    *passed += p;
    *total += t;
}

static void test_ipv6(unsigned int* passed, unsigned int* total) {
    unsigned int p = 0, t = 0;
    const char* name = "IPv6 addresses";
    char addr[IPAT_STRLEN_SAFE] = "";
    ipat_t ip = IPAT_INITIALIZE, ip2 = IPAT_INITIALIZE, * ipp = &ip, * ipp2 = &ip2;
    ipat_mac_t mac = IPAT_INITIALIZE, * macp = &mac;

    start(name);

    ipat_pton(ipp, AF_INET_ANY, "2001:db8::1:1");
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("2001:db8::1:1", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_CHAR("2001:db8::1:1/128", ipat_ntop_alloc(ipp, true), &p, &t);
    EXPECT_CHAR("2001:db8::1:1", ipat_ntop(ipp, addr, IPAT_STRLEN_IPV6, false), &p, &t);
    EXPECT_CHAR("2001:db8::1:1/128", ipat_ntop(ipp, addr, IPAT_STRLEN_IPV6_CIDR, true), &p, &t);
    EXPECT_INT(AF_INET6, ipat_family(ipp), &p, &t);
    EXPECT_INT(128, ipat_prefix_length(ipp), &p, &t);
    ipat_pton(ipp2, AF_INET_ANY, "2001:db8::1:2");
    EXPECT_INT(IPAT_COMPARE_LESS, ipat_compare(ipp, ipp2, false), &p, &t);
    EXPECT_INT(IPAT_COMPARE_GREATER, ipat_compare(ipp2, ipp, false), &p, &t);
    EXPECT_INT(IPAT_COMPARE_EQUAL, ipat_compare(ipp, ipp, true), &p, &t);

    ipat_pton(ipp, AF_INET_ANY, "2001:db8::1:1/112");
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("2001:db8::1:1", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_CHAR("2001:db8::1:1/112", ipat_ntop_alloc(ipp, true), &p, &t);
    EXPECT_INT(AF_INET6, ipat_family(ipp), &p, &t);
    EXPECT_INT(112, ipat_prefix_length(ipp), &p, &t);
    ipat_pton(ipp2, AF_INET_ANY, "2001:db8::1:2");
    EXPECT_INT(IPAT_COMPARE_LESS, ipat_compare(ipp, ipp2, false), &p, &t);
    EXPECT_INT(IPAT_COMPARE_GREATER, ipat_compare(ipp2, ipp, false), &p, &t);
    EXPECT_INT(IPAT_COMPARE_EQUAL, ipat_compare(ipp, ipp, true), &p, &t);
    ipat_pton(ipp2, AF_INET_ANY, "2001:db8::1:1/128");
    EXPECT_INT(IPAT_COMPARE_LESS, ipat_compare(ipp, ipp2, true), &p, &t);

    ipat_pton(ipp, AF_INET_ANY, "2001:db8::1:1/128");
    EXPECT_BOOL(true, ipat_mask_apply_direct(ipp, 112, ipat_bits_upper, ipat_oper_and), &p, &t);
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("2001:db8::1:0", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_CHAR("2001:db8::1:0/128", ipat_ntop_alloc(ipp, true), &p, &t);
    EXPECT_INT(AF_INET6, ipat_family(ipp), &p, &t);
    EXPECT_INT(128, ipat_prefix_length(ipp), &p, &t);
    EXPECT_BOOL(true, ipat_set_prefix_length(ipp, 112), &p, &t);
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("2001:db8::1:0", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_CHAR("2001:db8::1:0/112", ipat_ntop_alloc(ipp, true), &p, &t);
    EXPECT_INT(AF_INET6, ipat_family(ipp), &p, &t);
    EXPECT_INT(112, ipat_prefix_length(ipp), &p, &t);
    EXPECT_BOOL(true, ipat_mask_create(ipp2, AF_INET6, 96, ipat_bits_lower), &p, &t);
    EXPECT_BOOL(true, ipat_mask_apply(ipp, ipp2, ipat_oper_or), &p, &t);
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("2001:db8::ffff:ffff", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_CHAR("2001:db8::ffff:ffff/112", ipat_ntop_alloc(ipp, true), &p, &t);
    EXPECT_INT(AF_INET6, ipat_family(ipp), &p, &t);
    EXPECT_INT(112, ipat_prefix_length(ipp), &p, &t);

    ipat_pton(ipp2, AF_INET_ANY, "2001:db8::1:1/112");
    EXPECT_BOOL(true, ipat_to_subnet(ipp, ipp2), &p, &t);
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_INT(AF_INET6, ipat_family(ipp), &p, &t);
    EXPECT_CHAR("2001:db8::1:0", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_CHAR("2001:db8::1:0/112", ipat_ntop_alloc(ipp, true), &p, &t);
    EXPECT_INT(112, ipat_prefix_length(ipp), &p, &t);

    EXPECT_BOOL(true, ipat_pton(ipp, AF_INET_ANY, "::/0"), &p, &t);
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_INT(AF_INET6, ipat_family(ipp), &p, &t);
    EXPECT_INT(0, ipat_prefix_length(ipp), &p, &t);

    EXPECT_BOOL(false, ipat_pton(ipp, AF_INET_ANY, "::/"), &p, &t);

    ipat_pton(ipp, AF_INET_ANY, "2001:db8::1:1/128");
    ipat_pton(ipp2, AF_INET_ANY, "2001:db8::1:0/112");
    EXPECT_BOOL(true, ipat_in_subnet(ipp, ipp2), &p, &t);
    ipat_pton(ipp2, AF_INET_ANY, "192.168.2.0/112");
    EXPECT_BOOL(false, ipat_in_subnet(ipp, ipp2), &p, &t);

    ipat_pton(ipp, AF_INET_ANY, "2001:db8::/64");
    ipat_mac_pton(macp, "11:22:33:44:55:66");
    EXPECT_BOOL(true, ipat_make_EUI64_text(ipp, "11:22:33:44:55:66"), &p, &t);
    EXPECT_BOOL(true, ipat_is_EUI64_text(ipp, "11:22:33:44:55:66"), &p, &t);
    EXPECT_BOOL(true, ipat_is_EUI64(ipp, macp), &p, &t);
    EXPECT_CHAR("2001:db8::1322:33ff:fe44:5566", ipat_ntop_alloc(ipp, false), &p, &t);
    ipat_pton(ipp, AF_INET_ANY, "2001:db8::/64");
    EXPECT_BOOL(true, ipat_make_EUI64(ipp, macp), &p, &t);
    EXPECT_BOOL(true, ipat_is_EUI64_text(ipp, "11:22:33:44:55:66"), &p, &t);
    EXPECT_BOOL(true, ipat_is_EUI64(ipp, macp), &p, &t);
    EXPECT_CHAR("2001:db8::1322:33ff:fe44:5566", ipat_ntop_alloc(ipp, false), &p, &t);

    EXPECT_INT(-1, ipat_mask_prefix_length(AF_INET_ANY, "ffff::ffff:0000"), &p, &t);
    EXPECT_INT(-1, ipat_mask_prefix_length(AF_INET_ANY, "ffff::ff5f:0000"), &p, &t);
    EXPECT_INT(-1, ipat_mask_prefix_length(AF_INET_ANY, "ff5f::"), &p, &t);
    EXPECT_INT(0, ipat_mask_prefix_length(AF_INET_ANY, "::"), &p, &t);
    EXPECT_INT(16, ipat_mask_prefix_length(AF_INET_ANY, "ffff::"), &p, &t);
    EXPECT_INT(18, ipat_mask_prefix_length(AF_INET_ANY, "ffff:c000::"), &p, &t);
    EXPECT_INT(128, ipat_mask_prefix_length(AF_INET_ANY, "ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff"), &p, &t);
    EXPECT_BOOL(true, ipat_pton(ipp, AF_INET_ANY, "2001:db8::/ffff:c000::"), &p, &t);
    EXPECT_INT(AF_INET6, ipat_family(ipp), &p, &t);
    EXPECT_INT(18, ipat_prefix_length(ipp), &p, &t);
    EXPECT_BOOL(false, ipat_pton(ipp, AF_INET_ANY, "2001:db8::/255.0.0.0"), &p, &t);

    ipat_pton(ipp, AF_INET6, "::ffff:192.168.1.1");
    EXPECT_BOOL(true, ipat_convert(ipp, AF_INET), &p, &t);
    EXPECT_CHAR("192.168.1.1/32", ipat_ntop_alloc(ipp, true), &p, &t);

    verdict(name, p, t);
    *passed += p;
    *total += t;
}

static void test_ipv4(unsigned int* passed, unsigned int* total) {
    unsigned int p = 0, t = 0;
    const char* name = "IPv4 addresses";
    char addr[IPAT_STRLEN_SAFE] = "";
    ipat_t ip = IPAT_INITIALIZE, ip2 = IPAT_INITIALIZE, * ipp = &ip, * ipp2 = &ip2;

    start(name);

    ipat_pton(ipp, AF_INET_ANY, "192.168.1.1");
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("192.168.1.1", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_CHAR("192.168.1.1/32", ipat_ntop_alloc(ipp, true), &p, &t);
    EXPECT_CHAR("192.168.1.1", ipat_ntop(ipp, addr, IPAT_STRLEN_IPV4, false), &p, &t);
    EXPECT_CHAR("192.168.1.1/32", ipat_ntop(ipp, addr, IPAT_STRLEN_IPV4_CIDR, true), &p, &t);
    EXPECT_INT(AF_INET, ipat_family(ipp), &p, &t);
    EXPECT_INT(32, ipat_prefix_length(ipp), &p, &t);
    ipat_pton(ipp2, AF_INET_ANY, "192.168.1.2");
    EXPECT_INT(IPAT_COMPARE_LESS, ipat_compare(ipp, ipp2, false), &p, &t);
    EXPECT_INT(IPAT_COMPARE_GREATER, ipat_compare(ipp2, ipp, false), &p, &t);
    EXPECT_INT(IPAT_COMPARE_EQUAL, ipat_compare(ipp, ipp, true), &p, &t);

    ipat_pton(ipp, AF_INET_ANY, "192.168.1.1/24");
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("192.168.1.1", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_CHAR("192.168.1.1/24", ipat_ntop_alloc(ipp, true), &p, &t);
    EXPECT_INT(AF_INET, ipat_family(ipp), &p, &t);
    EXPECT_INT(24, ipat_prefix_length(ipp), &p, &t);
    ipat_pton(ipp2, AF_INET_ANY, "192.168.1.2");
    EXPECT_INT(IPAT_COMPARE_LESS, ipat_compare(ipp, ipp2, false), &p, &t);
    EXPECT_INT(IPAT_COMPARE_GREATER, ipat_compare(ipp2, ipp, false), &p, &t);
    EXPECT_INT(IPAT_COMPARE_EQUAL, ipat_compare(ipp, ipp, true), &p, &t);
    ipat_pton(ipp2, AF_INET_ANY, "192.168.1.1/32");
    EXPECT_INT(IPAT_COMPARE_LESS, ipat_compare(ipp, ipp2, true), &p, &t);

    ipat_pton(ipp, AF_INET_ANY, "192.168.1.1/32");
    EXPECT_BOOL(true, ipat_mask_apply_direct(ipp, 24, ipat_bits_upper, ipat_oper_and), &p, &t);
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("192.168.1.0", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_CHAR("192.168.1.0/32", ipat_ntop_alloc(ipp, true), &p, &t);
    EXPECT_INT(AF_INET, ipat_family(ipp), &p, &t);
    EXPECT_INT(32, ipat_prefix_length(ipp), &p, &t);
    EXPECT_BOOL(true, ipat_set_prefix_length(ipp, 24), &p, &t);
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("192.168.1.0", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_CHAR("192.168.1.0/24", ipat_ntop_alloc(ipp, true), &p, &t);
    EXPECT_INT(AF_INET, ipat_family(ipp), &p, &t);
    EXPECT_INT(24, ipat_prefix_length(ipp), &p, &t);
    EXPECT_BOOL(true, ipat_mask_create(ipp2, AF_INET, 16, ipat_bits_lower), &p, &t);
    EXPECT_BOOL(true, ipat_mask_apply(ipp, ipp2, ipat_oper_or), &p, &t);
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("192.168.255.255", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_CHAR("192.168.255.255/24", ipat_ntop_alloc(ipp, true), &p, &t);
    EXPECT_INT(AF_INET, ipat_family(ipp), &p, &t);
    EXPECT_INT(24, ipat_prefix_length(ipp), &p, &t);

    ipat_pton(ipp2, AF_INET_ANY, "192.168.1.1/24");
    EXPECT_BOOL(true, ipat_to_subnet(ipp, ipp2), &p, &t);
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("192.168.1.0", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_CHAR("192.168.1.0/24", ipat_ntop_alloc(ipp, true), &p, &t);
    EXPECT_INT(AF_INET, ipat_family(ipp), &p, &t);
    EXPECT_INT(24, ipat_prefix_length(ipp), &p, &t);

    EXPECT_BOOL(true, ipat_pton(ipp, AF_INET_ANY, "0.0.0.0/0"), &p, &t);
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_INT(AF_INET, ipat_family(ipp), &p, &t);
    EXPECT_INT(0, ipat_prefix_length(ipp), &p, &t);

    EXPECT_BOOL(false, ipat_pton(ipp, AF_INET_ANY, "0.0.0.0/"), &p, &t);

    ipat_pton(ipp, AF_INET_ANY, "192.168.1.1/32");
    ipat_pton(ipp2, AF_INET_ANY, "192.168.1.0/24");
    EXPECT_BOOL(true, ipat_in_subnet(ipp, ipp2), &p, &t);
    ipat_pton(ipp2, AF_INET_ANY, "192.168.2.0/24");
    EXPECT_BOOL(false, ipat_in_subnet(ipp, ipp2), &p, &t);

    EXPECT_INT(-1, ipat_mask_prefix_length(AF_INET_ANY, "255.0.255.0"), &p, &t);
    EXPECT_INT(-1, ipat_mask_prefix_length(AF_INET_ANY, "255.0.16.0"), &p, &t);
    EXPECT_INT(-1, ipat_mask_prefix_length(AF_INET_ANY, "255.197.0.0"), &p, &t);
    EXPECT_INT(0, ipat_mask_prefix_length(AF_INET_ANY, "0.0.0.0"), &p, &t);
    EXPECT_INT(16, ipat_mask_prefix_length(AF_INET_ANY, "255.255.0.0"), &p, &t);
    EXPECT_INT(18, ipat_mask_prefix_length(AF_INET_ANY, "255.255.192.0"), &p, &t);
    EXPECT_INT(32, ipat_mask_prefix_length(AF_INET_ANY, "255.255.255.255"), &p, &t);
    EXPECT_BOOL(true, ipat_pton(ipp, AF_INET_ANY, "192.168.1.0/255.255.192.0"), &p, &t);
    EXPECT_INT(AF_INET, ipat_family(ipp), &p, &t);
    EXPECT_INT(18, ipat_prefix_length(ipp), &p, &t);
    EXPECT_BOOL(false, ipat_pton(ipp, AF_INET_ANY, "192.168.1.0/ffff::"), &p, &t);

    ipat_pton(ipp, AF_INET, "192.168.1.1");
    EXPECT_BOOL(true, ipat_convert(ipp, AF_INET6), &p, &t);
    EXPECT_CHAR("::ffff:192.168.1.1/128", ipat_ntop_alloc(ipp, true), &p, &t);

    verdict(name, p, t);
    *passed += p;
    *total += t;
}

static void test_validity(unsigned int* passed, unsigned int* total) {
    void* buffer = NULL;
    int bufsize = 0;
    unsigned int p = 0, t = 0;
    const char* name = "validity";
    ipat_t ip = IPAT_INITIALIZE, * ipp = &ip;

    start(name);

    /* Static init */
    EXPECT_BOOL(false, ipat_valid(ipp), &p, &t);

    /* Init */
    ipat_initialize(ipp);
    EXPECT_BOOL(false, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR(NULL, ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_BOOL(false, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET_INVALID, ipat_family(ipp), &p, &t);
    EXPECT_INT(-1, ipat_prefix_length(ipp), &p, &t);
    EXPECT_BOOL(true, ipat_type(ipp, NULL) == IPAT_TYPE_INVALID, &p, &t);
    EXPECT_BOOL(false, ipat_mask_apply_direct(ipp, 16, ipat_bits_upper, ipat_oper_and), &p, &t);

    /* Set IPv4 address */
    ipat_pton(ipp, AF_INET, "123.123.123.123");
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("123.123.123.123", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_BOOL(false, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET, ipat_family(ipp), &p, &t);
    EXPECT_INT(32, ipat_prefix_length(ipp), &p, &t);
    EXPECT_BOOL(false, ipat_type(ipp, NULL) == IPAT_TYPE_INVALID, &p, &t);
    EXPECT_BOOL(true, ipat_mask_apply_direct(ipp, 16, ipat_bits_upper, ipat_oper_and), &p, &t);
    ipat_pton(ipp, AF_INET_ANY, "123.123.123.123");
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("123.123.123.123", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_BOOL(false, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET, ipat_family(ipp), &p, &t);
    EXPECT_INT(32, ipat_prefix_length(ipp), &p, &t);
    EXPECT_BOOL(false, ipat_type(ipp, NULL) == IPAT_TYPE_INVALID, &p, &t);
    EXPECT_BOOL(true, ipat_mask_apply_direct(ipp, 16, ipat_bits_upper, ipat_oper_and), &p, &t);

    /* Set IPv4 NUL directly */
    EXPECT_BOOL(true, ipat_set_nul(ipp, AF_INET), &p, &t);
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("0.0.0.0", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_BOOL(false, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET, ipat_family(ipp), &p, &t);
    EXPECT_INT(32, ipat_prefix_length(ipp), &p, &t);
    EXPECT_INT(IPAT_TYPE_NUL, (ipat_type(ipp, NULL) & IPAT_TYPE_NUL), &p, &t);

    /* Set IPv4 address directly */
    ipat_pton(ipp, AF_INET_ANY, "123.123.123.123");
    buffer = ipat_buffer(ipp, &bufsize);
    EXPECT_BOOL(true, ipat_set(ipp, AF_INET, buffer, bufsize), &p, &t);
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("123.123.123.123", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_BOOL(false, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET, ipat_family(ipp), &p, &t);
    EXPECT_INT(32, ipat_prefix_length(ipp), &p, &t);
    EXPECT_BOOL(false, ipat_type(ipp, NULL) == IPAT_TYPE_INVALID, &p, &t);
    EXPECT_BOOL(true, ipat_mask_apply_direct(ipp, 16, ipat_bits_upper, ipat_oper_and), &p, &t);

    /* Set IPv6 address */
    ipat_pton(ipp, AF_INET6, "2001:db8::1");
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("2001:db8::1", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_BOOL(false, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET6, ipat_family(ipp), &p, &t);
    EXPECT_INT(128, ipat_prefix_length(ipp), &p, &t);
    EXPECT_BOOL(false, ipat_type(ipp, NULL) == IPAT_TYPE_INVALID, &p, &t);
    EXPECT_BOOL(true, ipat_mask_apply_direct(ipp, 16, ipat_bits_upper, ipat_oper_and), &p, &t);
    ipat_pton(ipp, AF_INET_ANY, "2001:db8::1");
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("2001:db8::1", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_BOOL(false, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET6, ipat_family(ipp), &p, &t);
    EXPECT_INT(128, ipat_prefix_length(ipp), &p, &t);
    EXPECT_BOOL(false, ipat_type(ipp, NULL) == IPAT_TYPE_INVALID, &p, &t);
    EXPECT_BOOL(true, ipat_mask_apply_direct(ipp, 16, ipat_bits_upper, ipat_oper_and), &p, &t);

    /* Set IPv6 NUL directly */
    EXPECT_BOOL(true, ipat_set_nul(ipp, AF_INET6), &p, &t);
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("::", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_BOOL(false, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET6, ipat_family(ipp), &p, &t);
    EXPECT_INT(128, ipat_prefix_length(ipp), &p, &t);
    EXPECT_INT(IPAT_TYPE_NUL, (ipat_type(ipp, NULL) & IPAT_TYPE_NUL), &p, &t);

    /* Set IPv6 address directly */
    ipat_pton(ipp, AF_INET_ANY, "2001:db8::1");
    buffer = ipat_buffer(ipp, &bufsize);
    EXPECT_BOOL(true, ipat_set(ipp, AF_INET6, buffer, bufsize), &p, &t);
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR("2001:db8::1", ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_BOOL(false, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET6, ipat_family(ipp), &p, &t);
    EXPECT_INT(128, ipat_prefix_length(ipp), &p, &t);
    EXPECT_BOOL(false, ipat_type(ipp, NULL) == IPAT_TYPE_INVALID, &p, &t);
    EXPECT_BOOL(true, ipat_mask_apply_direct(ipp, 16, ipat_bits_upper, ipat_oper_and), &p, &t);

    /* Set any IPv4 */
    ipat_set_any(ipp, AF_INET);
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR(ipat_any4, ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_BOOL(true, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET, ipat_family(ipp), &p, &t);
    EXPECT_INT(-1, ipat_prefix_length(ipp), &p, &t);
    EXPECT_BOOL(false, ipat_type(ipp, NULL) == IPAT_TYPE_INVALID, &p, &t);
    EXPECT_BOOL(false, ipat_mask_apply_direct(ipp, 16, ipat_bits_upper, ipat_oper_and), &p, &t);

    /* Set any IPv6 */
    ipat_set_any(ipp, AF_INET6);
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR(ipat_any6, ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_BOOL(true, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET6, ipat_family(ipp), &p, &t);
    EXPECT_INT(-1, ipat_prefix_length(ipp), &p, &t);
    EXPECT_BOOL(false, ipat_type(ipp, NULL) == IPAT_TYPE_INVALID, &p, &t);
    EXPECT_BOOL(false, ipat_mask_apply_direct(ipp, 16, ipat_bits_upper, ipat_oper_and), &p, &t);

    /* Set any IPvAny */
    ipat_set_any(ipp, AF_INET_ANY);
    EXPECT_BOOL(true, ipat_valid(ipp), &p, &t);
    EXPECT_CHAR(ipat_any, ipat_ntop_alloc(ipp, false), &p, &t);
    EXPECT_BOOL(true, ipat_is_any(ipp), &p, &t);
    EXPECT_INT(AF_INET_ANY, ipat_family(ipp), &p, &t);
    EXPECT_INT(-1, ipat_prefix_length(ipp), &p, &t);
    EXPECT_BOOL(false, ipat_type(ipp, NULL) == IPAT_TYPE_INVALID, &p, &t);
    EXPECT_BOOL(false, ipat_mask_apply_direct(ipp, 16, ipat_bits_upper, ipat_oper_and), &p, &t);

    /* Cleanup */
    ipat_cleanup(ipp);
    EXPECT_BOOL(false, ipat_valid(ipp), &p, &t);

    verdict(name, p, t);
    *passed += p;
    *total += t;
}

static void test_sizes(unsigned int* passed, unsigned int* total) {
    unsigned int p = 0, t = 0;
    const char* name = "sizes";

    start(name);

    /* Max size */
    EXPECT_INT(IPAT_SIZE_IPV4, ipat_max_size(AF_INET), &p, &t);
    EXPECT_INT(IPAT_SIZE_IPV6, ipat_max_size(AF_INET6), &p, &t);
    EXPECT_INT(-1, ipat_max_size(AF_INET_ANY), &p, &t);
    EXPECT_INT(-1, ipat_max_size(AF_INET_INVALID), &p, &t);
    EXPECT_INT(-1, ipat_max_size(999), &p, &t);

    /* Max prefix length */
    EXPECT_INT(IPAT_PREFIXLEN_IPV4, ipat_max_prefix_length(AF_INET), &p, &t);
    EXPECT_INT(IPAT_PREFIXLEN_IPV6, ipat_max_prefix_length(AF_INET6), &p, &t);
    EXPECT_INT(-1, ipat_max_prefix_length(AF_INET_ANY), &p, &t);
    EXPECT_INT(-1, ipat_max_prefix_length(AF_INET_INVALID), &p, &t);
    EXPECT_INT(-1, ipat_max_prefix_length(999), &p, &t);

    /* Min string length */
    EXPECT_INT(IPAT_STRLEN_IPV4, ipat_min_string_length(AF_INET, false), &p, &t);
    EXPECT_INT(IPAT_STRLEN_IPV4_CIDR, ipat_min_string_length(AF_INET, true), &p, &t);
    EXPECT_INT(IPAT_STRLEN_IPV6, ipat_min_string_length(AF_INET6, false), &p, &t);
    EXPECT_INT(IPAT_STRLEN_IPV6_CIDR, ipat_min_string_length(AF_INET6, true), &p, &t);
    EXPECT_INT(-1, ipat_min_string_length(AF_INET_ANY, false), &p, &t);
    EXPECT_INT(-1, ipat_min_string_length(AF_INET_ANY, true), &p, &t);
    EXPECT_INT(-1, ipat_min_string_length(AF_INET_INVALID, false), &p, &t);
    EXPECT_INT(-1, ipat_min_string_length(AF_INET_INVALID, true), &p, &t);
    EXPECT_INT(-1, ipat_min_string_length(999, false), &p, &t);
    EXPECT_INT(-1, ipat_min_string_length(999, true), &p, &t);

    /* Family compatibility */
    EXPECT_BOOL(true, ipat_compatible_family(AF_INET, AF_INET), &p, &t);
    EXPECT_BOOL(true, ipat_compatible_family(AF_INET6, AF_INET6), &p, &t);
    EXPECT_BOOL(true, ipat_compatible_family(AF_INET, AF_INET_ANY), &p, &t);
    EXPECT_BOOL(true, ipat_compatible_family(AF_INET6, AF_INET_ANY), &p, &t);
    EXPECT_BOOL(true, ipat_compatible_family(AF_INET_ANY, AF_INET_ANY), &p, &t);
    EXPECT_BOOL(false, ipat_compatible_family(AF_INET, AF_INET6), &p, &t);
    EXPECT_BOOL(false, ipat_compatible_family(AF_INET, AF_INET_INVALID), &p, &t);
    EXPECT_BOOL(false, ipat_compatible_family(AF_INET6, AF_INET_INVALID), &p, &t);
    EXPECT_BOOL(false, ipat_compatible_family(AF_INET_ANY, AF_INET_INVALID), &p, &t);
    EXPECT_BOOL(false, ipat_compatible_family(AF_INET_INVALID, AF_INET), &p, &t);
    EXPECT_BOOL(false, ipat_compatible_family(AF_INET_INVALID, AF_INET6), &p, &t);
    EXPECT_BOOL(false, ipat_compatible_family(AF_INET_INVALID, AF_INET_ANY), &p, &t);

    verdict(name, p, t);
    *passed += p;
    *total += t;
}

void test_basis(unsigned int* passed, unsigned int* total) {
    unsigned int p = 0, t = 0;
    const char* name = "basis";

    start(name);

    test_sizes(&p, &t);
    test_validity(&p, &t);
    test_ipv4(&p, &t);
    test_ipv6(&p, &t);
    test_any(&p, &t);

    verdict(name, p, t);
    *passed += p;
    *total += t;
}
