-include $(CONFIGDIR)/components.config

export INSTALL ?= install
export PKG_CONFIG_LIBDIR ?= /usr/lib/pkgconfig
export BINDIR ?= /usr/bin
export LIBDIR ?= /usr/lib
export SLIBDIR ?= /usr/lib
export LUALIBDIR ?= /usr/lib/lua
export INCLUDEDIR ?= /usr/include
export INITDIR ?= /etc/init.d
export ACLDIR ?= /etc/acl
export DOCDIR ?= $(D)/usr/share/doc/libipat
export PROCMONDIR ?= /usr/lib/processmonitor/scripts
export RESETDIR ?= /etc/reset
export MACHINE ?= $(shell $(CC) -dumpmachine)

export COMPONENT = libipat

compile:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C doc clean

install:
	$(INSTALL) -d -m 0755 $(D)/$(INCLUDEDIR)/ipat
	$(INSTALL) -D -p -m 0644 include/ipat/*.h $(D)$(INCLUDEDIR)/ipat/
	$(INSTALL) -D -p -m 0755 src/$(COMPONENT).so $(D)$(LIBDIR)/$(COMPONENT).so.$(PV)
	ln -sfr $(D)$(LIBDIR)/$(COMPONENT).so.$(PV) $(D)$(LIBDIR)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 src/$(COMPONENT).a $(D)$(LIBDIR)/$(COMPONENT).a
	$(INSTALL) -D -p -m 0644 pkgconfig/ipat.pc $(PKG_CONFIG_LIBDIR)/ipat.pc

.PHONY: compile clean install

