/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _IPAT_IPAT_H_
#define _IPAT_IPAT_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @file ipat.h
 * @brief IPAT IP functions.
 *
 * The IPAT IP library currently supports the following IP family macros:
 * - AF_INET for the IPv4 family
 * - AF_INET6 for the IPv6 family
 * - AF_INET_ANY for any IP family
 * - AF_INET_INVALID as an indication of an invalid IP family
 *
 * @note
 * Upon failure of a function, errno will be set indicating the type of error that occured.
 * Following errno values are used:
 * - EINVAL       : An invalid argument, NULL pointer, uninitialized IPAT (MAC) structure, internal error...
 * - ENOMEM       : An out-of-memory condition occured when allocating memory.
 * - ENOAFSUPPORT : An invalid IP family was provided as argument to the function.
 * The "failure" return value of some functions can be a valid return value as well as an indication
 * that a real error occured.
 * Eg: ipat_valid(ip) sets errno to EINVAL if 'ip' is a NULL pointer and returns false, which is also returned
 * when a true IPAT structure is not valid (no IP address set).
 * Checking errno can help distinguish between the 2 cases.
 * If such distinguishment is required, it's necessary to set errno to 0 (zero) before invoking the function.
 * No function will set errno to 0 (zero) upon success.
 */

#include <ipat/ipat_def.h>
#include <ipat/ipat_text.h>
#include <ipat/ipat_mac.h>
#include <ipat/ipat_mac_text.h>

/**
 * Initialize an IPAT structure.
 * Required to do before using it.
 * @param ip IPAT structure
 * @return True for success
 */
bool ipat_initialize(ipat_t* ip);

/**
 * Clean up an IPAT structure.
 * Required to do after usage.
 * @param ip IPAT structure
 * @return Void
 */
void ipat_cleanup(ipat_t* ip);

/**
 * Copy the source to the destination.
 * @param dst IPAT destination structure
 * @param src IPAT source structure
 * @return True for success
 */
bool ipat_copy(ipat_t* dst, const ipat_t* src);

/**
 * Retrieve the storage size for
 * an IP address in the given family.
 * @param family Must be one of the supported families
 * @return Storage size or -1 upon failure
 */
int ipat_max_size(int family);

/**
 * Retrieve the maximum prefix length
 * for an IP address in the given family.
 * @param family Must be one of the supported families
 * @return Maximum prefix length or -1 upon failure
 */
int ipat_max_prefix_length(int family);

/**
 * Retrieve the minimum string buffer length
 * for an IP address in the given family.
 * When CIDR notation is requested, make sure
 * the length can also hold the "/len" suffix.
 * @param family Must be one of the supported families
 * @param cidr CIDR notation requested
 * @return The minimum length or -1 upon failure
 */
int ipat_min_string_length(int family, bool cidr);

/**
 * Check if an IP family is supported.
 * This takes the AF_INET_ANY family into account, too.
 * @param family Must be one of the supported families
 * @param allow_any Allow AF_INET_ANY family
 * @return True for success
 */
bool ipat_supported_family(int family, bool allow_any);

/**
 * Check if 2 IP families are compatible with each other.
 * This takes the AF_INET_ANY family into account, too.
 * @param family1 Must be one of the supported families
 * @param family2 Must be one of the supported families
 * @return True for success
 */
bool ipat_compatible_family(int family1, int family2);

/**
 * Perform a combination operation upon 2 IP families.
 * This takes the AF_INET_ANY family into account, too.
 * This takes the AF_INET_INVALID family into account, too.
 * The return value AF_INET_INVALID can be valid. To check
 * for errors, clear errno first and check errno afterwards.
 * @param family1 Must be one of the supported families
 * @param family2 Must be one of the supported families
 * @param oper Defines the operation to use
 * @return The combined family or AF_INET_INVALID upon failure
 */
int ipat_combine_family(int family1, int family2, ipat_oper_t oper);

/**
 * Check if an IPAT structure is valid.
 * This means, is there an IP address stored and
 * can the IPAT structure be used with other functions.
 * @param ip IPAT structure
 * @return True for success
 */
bool ipat_valid(const ipat_t* ip);

/**
 * Convert an address in text format in the given family
 * into an IPAT structure.
 * If family is AF_INET_ANY, then the appropriate family
 * will be deducted automatically.
 * The address can have a suffix, separated by the
 * '/'-character, to indicate the prefix length.
 * The suffix should be in the format of either
 * - A number (CIDR notation)
 * - An address mask in text format (eg: 255.0.0.0)
 * If no prefix length is provided, then the maximum
 * family prefix length is assumed.
 * @param ip IPAT structure
 * @param family The requested family, or AF_INET_ANY
 * @param src The IP address in text format
 * @return True for success
 */
bool ipat_pton(ipat_t* ip, int family, const char* src);

/**
 * Convert an IPAT structure into text format.
 * The result is stored in buffer dst of length dstlen,
 * which must be large enough to store the result.
 * If CIDR notation is requested, the "/len" suffix
 * is appended.
 * @param ip IPAT structure
 * @param dst Buffer to store the result
 * @param dstlen The buffer's length
 * @param cidr CIDR notation requested
 * @return Pointer to the resulting buffer or NULL upon failure
 */
char* ipat_ntop(const ipat_t* ip, char* dst, int dstlen, bool cidr);

/**
 * Convert an IPAT structure into text format.
 * A sufficiently large buffer is allocated
 * on the heap, in which case it must be freed
 * by the caller after usage.
 * If CIDR notation is requested, the "/len" suffix
 * is appended.
 * @param ip IPAT structure
 * @param cidr CIDR notation requested
 * @return Pointer to the resulting buffer or NULL upon failure
 */
char* ipat_ntop_alloc(const ipat_t* ip, bool cidr);

/**
 * Fill in the IPAT structure with the IP address
 * provided in its binary format.
 * @param ip IPAT structure
 * @param family Must be one of the supported families
 * @param src Pointer to the address in binary format
 * @param srclen The length of the address
 * @return True for success
 */
bool ipat_set(ipat_t* ip, int family, const void* src, int srclen);

/**
 * Fill in the IPAT structure with the NUL IP address.
 * @param ip IPAT structure
 * @param family Must be one of the supported families
 * @return True for success
 */
bool ipat_set_nul(ipat_t* ip, int family);

/**
 * Set the IPAT structure as an any address.
 * @param ip IPAT structure
 * @param family Must be one of the supported families including AF_INET_ANY
 * @return True for success
 */
bool ipat_set_any(ipat_t* ip, int family);

/**
 * Check whether the IPAT structure is an any address.
 * @param ip IPAT structure
 * @return True for success
 */
bool ipat_is_any(const ipat_t* ip);

/**
 * Retrieve the current family of the IPAT structure.
 * @param ip IPAT structure
 * @return The family or AF_INET_INVALID upon failure
 */
int ipat_family(const ipat_t* ip);

/**
 * Retrieve a copy of the internal buffer storing
 * the IP address in binary format.
 * This buffer must be freed after usage.
 * If size is not NULL, the size of the returned
 * buffer is stored there.
 * @param ip IPAT structure
 * @param size Pointer to integer for returning the size
 * @return A copy of the internal buffer or NULL upon failure
 */
void* ipat_buffer(const ipat_t* ip, int* size);

/**
 * Retrieve a pointer to the internal buffer storing
 * the IP address in binary format.
 * This buffer must not be altered!
 * If size is not NULL, the size of the returned
 * buffer is stored there.
 * @param ip IPAT structure
 * @param size Pointer to integer for returning the size
 * @return A pointer to the internal buffer or NULL upon failure
 */
void* ipat_da_buffer(ipat_t* ip, int* size);

/**
 * Retrieve the prefix length of the IPAT structure.
 * @param ip IPAT structure
 * @return The prefix length
 */
int ipat_prefix_length(const ipat_t* ip);

/**
 * Set the prefix length of the IPAT structure.
 * @param ip IPAT structure
 * @param prefixlen The new prefix length
 * @return True for success
 */
bool ipat_set_prefix_length(ipat_t* ip, int prefixlen);

/**
 * Retrieve a bitmask indicating the type of
 * of address of the IPAT structure.
 * If family is not NULL, the value is set with a
 * bitmask of family specific address types.
 * @param ip IPAT structure
 * @param family Pointer to integer for family specific bitmask
 * @return A bitmask or IPAT_TYPE_INVALID upon failure
 */
unsigned int ipat_type(const ipat_t* ip, unsigned int* family);

/**
 * Create an IP mask for the given family with length len.
 * This will set either the upper or the lower bits
 * of the IPAT structure.
 * @param ip IPAT structure
 * @param family Must be one of the supported families
 * @param len The length of the requested IP mask
 * @param bits Defines which bits to set
 * @return True for success
 */
bool ipat_mask_create(ipat_t* ip, int family, int len, ipat_bits_t bits);

/**
 * Apply an IP mask to an IP address.
 * The operation can be AND, OR or XOR.
 * @param ip IPAT structure
 * @param mask IPAT structure containing the IP mask
 * @param oper Defines the operation to use
 * @return True for success
 */
bool ipat_mask_apply(ipat_t* ip, const ipat_t* mask, ipat_oper_t oper);

/**
 * Apply an IP mask to an IP address directly.
 * This is a combination of ipat_mask_create() and
 * ipat_mask_apply().
 * @param ip IPAT structure
 * @param len The length of the requested IP mask
 * @param bits Defines which bits to set
 * @param oper Defines the operation to use
 * @return True for success
 */
bool ipat_mask_apply_direct(ipat_t* ip, int len, ipat_bits_t bits, ipat_oper_t oper);

/**
 * Extract a subnet from an IPAT structure.
 * This is a combination of ipat_copy() and ipat_mask_apply_direct().
 * @param subnet IPAT structure
 * @param ip IPAT structure with the information
 * @return True for success
 */
bool ipat_to_subnet(ipat_t* subnet, const ipat_t* ip);

/**
 * Compare 2 IPAT structures.
 * If prefixlen is set, then the prefix length
 * of both IPAT structures is also compared.
 * @param a IPAT structure 1
 * @param b IPAT structure 2
 * @param prefixlen Indicates whether to check prefix lengths, too
 * @return IPAT_COMPARE_LESS, IPAT_COMPARE_EQUAL or IPAT_COMPARE_GREATER, or IPAT_COMPARE_ERROR upon failure
 */
int ipat_compare(const ipat_t* a, const ipat_t* b, bool prefixlen);

/**
 * Check if an IP address fits within a subnet.
 * @param ip IPAT structure
 * @param subnet IPAT structure with subnet information
 * @return True if it fits
 */
bool ipat_in_subnet(const ipat_t* ip, const ipat_t* subnet);

/**
 * Fill in the host part of an IPv6 IPAT structure
 * with the EUI-64 formatted MAC-address.
 * @param ip IPAT structure
 * @param mac The IPAT MAC structure
 * @return True for success
 */
bool ipat_make_EUI64(ipat_t* ip, const ipat_mac_t* mac);

/**
 * Check if the host part is an IPv6 IPAT structure
 * if formed by the EUI-64 formatted MAC-address.
 * @param ip IPAT structure
 * @param mac The IPAT MAC structure
 * @return True for success
 */
bool ipat_is_EUI64(const ipat_t* ip, const ipat_mac_t* mac);

/**
 * Fill in the host part of an IPv6 IPAT structure
 * with the EUI-64 formatted MAC-address.
 * The MAC address is parsed like the IPAT MAC parsing.
 * @param ip IPAT structure
 * @param mac The MAC address
 * @return True for success
 */
bool ipat_make_EUI64_text(ipat_t* ip, const char* mac);

/**
 * Check if the host part is an IPv6 IPAT structure
 * if formed by the EUI-64 formatted MAC-address.
 * The MAC address is parsed like the IPAT MAC parsing.
 * @param ip IPAT structure
 * @param mac The MAC address
 * @return True for success
 */
bool ipat_is_EUI64_text(const ipat_t* ip, const char* mac);

/**
 * Return the prefix length as given by the mask
 * IP address in text format.
 * Only the upper bits of the mask may be set.
 * @param family The requested family, or AF_INET_ANY
 * @param mask The mask IP address in text format
 * @return The prefix length, or -1 upon failure
 */
int ipat_mask_prefix_length(int family, const char* mask);

/**
 * Convert an IP address from one family to another.
 * Not all IP addresses can be converted.
 * An IPv4 address can always be formatted into an IPv6
 * address using the ::ffff:0:0/96 prefix.
 * Only an IPv6 address with the ::ffff:0:0/96 prefix can
 * be converted into an IPv4 address.
 * @param ip IPAT structure
 * @param to_family The family to convert to
 * @return True for success
 */
bool ipat_convert(ipat_t* ip, int to_family);

#ifdef __cplusplus
}
#endif

#endif
