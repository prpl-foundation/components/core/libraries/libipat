/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _IPAT_IPAT_MAC_TEXT_H_
#define _IPAT_IPAT_MAC_TEXT_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @file ipat_mac_text.h
 * @brief IPAT MAC Text functions.
 *
 * @note
 * The following functions are equivalent to the non-text
 * counterparts, but act upon text-formatted addresses.
 * The addresses can be formatted with or without separator.
 * The returned values of following functions must be freed
 * after usage:
 * - ipat_mac_text_copy()
 * - ipat_mac_text_buffer()
 * - ipat_mac_text_set()
 * - ipat_mac_text_set_nul()
 * - ipat_mac_text_get_part()
 * - ipat_mac_text_set_part()
 * - ipat_mac_text_set_unicast()
 * - ipat_mac_text_set_local()
 * - ipat_mac_text_mask_create()
 * - ipat_mac_text_mask_apply()
 * - ipat_mac_text_mask_apply_direct()
 * The 'sep' argument to some of these functions indicate
 * which separator must be used for the returned MAC address.
 * The separator must conform to the ipat_mac_sep_t enum.
 * The 'upper' argument to some of these functions indicate
 * whether the returned MAC address must be in
 * upper-case or lower-case.
 */

#include <ipat/ipat_mac.h>

char* ipat_mac_text_copy(const char* mact, ipat_mac_sep_t sep, bool upper);
bool ipat_mac_text_valid(const char* mact);
void* ipat_mac_text_buffer(const char* mact);
char* ipat_mac_text_set(void* src, int srclen, ipat_mac_sep_t sep, bool upper);
char* ipat_mac_text_set_nul(ipat_mac_sep_t sep);
bool ipat_mac_text_get_part(const char* mact, void* dst, int dstlen, ipat_mac_part_t part);
char* ipat_mac_text_set_part(const char* mact, void* src, int srclen, ipat_mac_part_t part, ipat_mac_sep_t sep, bool upper);
bool ipat_mac_text_is_unicast(const char* mact);
char* ipat_mac_text_set_unicast(const char* mact, bool unicast, ipat_mac_sep_t sep, bool upper);
bool ipat_mac_text_is_local(const char* mact);
char* ipat_mac_text_set_local(const char* mact, bool local, ipat_mac_sep_t sep, bool upper);
char* ipat_mac_text_mask_create(int len, ipat_bits_t bits, ipat_mac_sep_t sep, bool upper);
char* ipat_mac_text_mask_apply(const char* mact, const char* mask, ipat_oper_t oper, ipat_mac_sep_t sep, bool upper);
char* ipat_mac_text_mask_apply_direct(const char* mact, int len, ipat_bits_t bits, ipat_oper_t oper, ipat_mac_sep_t sep, bool upper);
int ipat_mac_text_compare(const char* a, const char* b, ipat_mac_part_t part);

#ifdef __cplusplus
}
#endif

#endif
