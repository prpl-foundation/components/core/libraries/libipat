/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _IPAT_IPAT_DEF_H_
#define _IPAT_IPAT_DEF_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @file ipat_def.h
 * @brief IPAT definitions.
 */

#include <stdbool.h>

/*
 * IPAT IP definitions
 */

#include <arpa/inet.h>

/*
 * Currently supported families are:
 * - AF_INET  : For IPv4
 * - AF_INET6 : For IPv6
 * Additions to AF_INET and AF_INET6 are defined below:
 */
#define AF_INET_INVALID         (-1)                    /**< @showinitializer Invalid IP family */
#define AF_INET_ANY             (0)                     /**< @showinitializer Any IP family */

/* IPAT IP string length macros */
#define IPAT_STRLEN_IPV4        (INET_ADDRSTRLEN)             /**< IPv4 address minimal string buffer size */
#define IPAT_STRLEN_IPV4_CIDR   (IPAT_STRLEN_IPV4 + 3)        /**< IPv4 address minimal string buffer size, CIDR notation: "/nn" [0 <= nn <= 32] */
#define IPAT_STRLEN_IPV6        (INET6_ADDRSTRLEN)            /**< IPv6 address minimal string buffer size */
#define IPAT_STRLEN_IPV6_CIDR   (IPAT_STRLEN_IPV6 + 4)        /**< IPv6 address minimal string buffer size, CIDR notation: "/nnn" [0 <= nnn <= 128] */
#define IPAT_STRLEN_ANY         ((int) strlen(ipat_any6) + 1) /**< 'Any' address minimal string buffer size */
#define IPAT_STRLEN_SAFE        (IPAT_STRLEN_IPV6_CIDR)       /**< Safe IP address minimal string buffer size for any families and notations */

/* IPAT IP storage size macros */
#define IPAT_SIZE_IPV4          ((int) sizeof(struct in_addr))  /**< IPv4 address storage size */
#define IPAT_SIZE_IPV6          ((int) sizeof(struct in6_addr)) /**< IPv6 address storage size */
#define IPAT_SIZE_SAFE          (IPAT_SIZE_IPV6)                /**< Safe IP address storage size for any families */

/* IPAT IP prefix length macros */
#define IPAT_PREFIXLEN_IPV4     (IPAT_SIZE_IPV4 * 8)      /**< IPv4 address maximum prefix length */
#define IPAT_PREFIXLEN_IPV6     (IPAT_SIZE_IPV6 * 8)      /**< IPv6 address maximum prefix length */

/* IP address types */
#define IPAT_TYPE_NONE          0x00000000              /**< No type known */
#define IPAT_TYPE_INVALID       0x00000001              /**< Invalid IP address */
#define IPAT_TYPE_IPVANY        0x00000002              /**< Any IP address family */
#define IPAT_TYPE_IPV4          0x00000004              /**< IPv4 address family */
#define IPAT_TYPE_IPV6          0x00000008              /**< IPv6 address family */
#define IPAT_TYPE_ANY           0x00000010              /**< Any IP address */
#define IPAT_TYPE_RESERVED      0x00000020              /**< Reserved IP address */
#define IPAT_TYPE_DOC           0x00000040              /**< Address used for documentation */
#define IPAT_TYPE_NUL           0x00000080              /**< The nul IP address */
#define IPAT_TYPE_UNICAST       0x00000100              /**< Unicast IP address */
#define IPAT_TYPE_ANYCAST       0x00000200              /**< Anycast IP address */
#define IPAT_TYPE_MULTICAST     0x00000400              /**< Multicast IP address */
#define IPAT_TYPE_BROADCAST     0x00000800              /**< Broadcast IP address */
#define IPAT_TYPE_HOST          0x00001000              /**< Most likely a host */
#define IPAT_TYPE_SUBNET        0x00002000              /**< Most likely a subnet range */
#define IPAT_TYPE_MASK          0x00004000              /**< Most likely a mask */
#define IPAT_TYPE_LOOPBACK      0x00008000              /**< Loopback address */
#define IPAT_TYPE_PUBLIC        0x00010000              /**< Public address */
#define IPAT_TYPE_PRIVATE       0x00020000              /**< Private address */
#define IPAT_TYPE_LINKLOCAL     0x00040000              /**< Link-local address */
#define IPAT_TYPE_SPECIAL       0x00080000              /**< Special scope, check family bitmask */

/* IP address type masks */
/** Mask for IP families */
#define IPAT_TYPE_MASK_FAMILY   (IPAT_TYPE_IPVANY | IPAT_TYPE_IPV4 | IPAT_TYPE_IPV6)
/** Mask for cast types */
#define IPAT_TYPE_MASK_CAST     (IPAT_TYPE_UNICAST | IPAT_TYPE_ANYCAST | IPAT_TYPE_MULTICAST | IPAT_TYPE_BROADCAST)
/** Mask for address types */
#define IPAT_TYPE_MASK_TYPE     (IPAT_TYPE_NUL | IPAT_TYPE_HOST | IPAT_TYPE_SUBNET | IPAT_TYPE_MASK)
/** Mask for scopes */
#define IPAT_TYPE_MASK_SCOPE    (IPAT_TYPE_RESERVED | IPAT_TYPE_DOC | IPAT_TYPE_LOOPBACK | IPAT_TYPE_PUBLIC | IPAT_TYPE_PRIVATE | IPAT_TYPE_LINKLOCAL | IPAT_TYPE_SPECIAL)

/* IPv4 specific address types */
#define IPAT_TYPE_IPV4_CURRENT      0x00000001          /**< Current network 0.0.0.0/8 */
#define IPAT_TYPE_IPV4_PRIVATE_24   0x00000002          /**< Private network 10.0.0.0/8 */
#define IPAT_TYPE_IPV4_SHARED       0x00000004          /**< Shared address space 100.64.0.0/10  (CG-NAT) */
#define IPAT_TYPE_IPV4_LOOPBACK     0x00000008          /**< Loopback 127.0.0.0/8 */
#define IPAT_TYPE_IPV4_LINKLOCAL    0x00000010          /**< Link-local 169.254.0.0/16 */
#define IPAT_TYPE_IPV4_PRIVATE_20   0x00000020          /**< Private network 172.16.0.0/12 */
#define IPAT_TYPE_IPV4_SPECIAL      0x00000040          /**< IETF Protocol Assignments 192.0.0.0/24 */
#define IPAT_TYPE_IPV4_DOC_1        0x00000080          /**< Documentation 192.0.2.0/24 */
#define IPAT_TYPE_IPV4_6TO4RELAY    0x00000100          /**< IPv6-to-IPv4 relay 192.88.99.0/24 */
#define IPAT_TYPE_IPV4_PRIVATE_16   0x00000200          /**< Private network 192.168.0.0/16 */
#define IPAT_TYPE_IPV4_BENCHMARK    0x00000400          /**< Network benchmark test 198.18.0.0/15 */
#define IPAT_TYPE_IPV4_DOC_2        0x00000800          /**< Documentation 198.51.100.0/24 */
#define IPAT_TYPE_IPV4_DOC_3        0x00001000          /**< Documentation 203.0.113.0/24 */
#define IPAT_TYPE_IPV4_MULTICAST    0x00002000          /**< IP multicast 224.0.0.0/4 */
#define IPAT_TYPE_IPV4_RESERVED     0x00004000          /**< Reserved 240.0.0.0/4 */
#define IPAT_TYPE_IPV4_BROADCAST    0x00008000          /**< Broadcast 255.255.255.255/32 */
#define IPAT_TYPE_IPV4_PUBLIC       0x01000000          /**< Public addres (custom) */

/* IPv6 specific address types */
#define IPAT_TYPE_IPV6_UNSPECIFIED  0x00000001          /**< Unspecified ::/128 */
#define IPAT_TYPE_IPV6_LOOPBACK     0x00000002          /**< Loopback ::1/128 */
#define IPAT_TYPE_IPV6_IPV4MAPPED   0x00000004          /**< IPv4 mapped address ::ffff:0:0/96 */
#define IPAT_TYPE_IPV6_DISCARD      0x00000008          /**< Discard prefix 100::/64 */
#define IPAT_TYPE_IPV6_4TO6TRANSL   0x00000010          /**< IPv4/IPv6 Translation 64:ff9b::/96 */
#define IPAT_TYPE_IPV6_TEREDO       0x00000020          /**< Teredo tunneling 2001::/32 */
#define IPAT_TYPE_IPV6_ORCHID       0x00000040          /**< ORCHID 2001:10::/28 */
#define IPAT_TYPE_IPV6_ORCHID2      0x00000080          /**< ORCHIDv2 2001:20::/28 */
#define IPAT_TYPE_IPV6_DOC          0x00000100          /**< Documentation 2001:db8::/32 */
#define IPAT_TYPE_IPV6_6TO4         0x00000200          /**< 6to4 2002::/16 */
#define IPAT_TYPE_IPV6_ULA          0x00000400          /**< Unique Local Address fc00::/7 */
#define IPAT_TYPE_IPV6_LLA          0x00000800          /**< Link-Local Address fe80::/10 */
#define IPAT_TYPE_IPV6_MULTICAST    0x00001000          /**< Multicast address ff00::/8 */
#define IPAT_TYPE_IPV6_GUA          0x01000000          /**< Global Unicast Address (custom) */
#define IPAT_TYPE_IPV6_EUI64        0x02000000          /**< Most likely modified EUI-64 address (custom) */
#define IPAT_TYPE_IPV6_MC_SCOPE     0xf0000000          /**< Multicast scope (custom, not a bitmask, see IPAT_TYPE_IPV6_MCSCOPE_ macros) */

/* IPv6 address multicast scopes */
#define IPAT_TYPE_IPV6_MCSCOPE_RESERVED0    0x00000000  /**< Reserved IPv6 multicast scope */
#define IPAT_TYPE_IPV6_MCSCOPE_INTERFACE    0x10000000  /**< Interface IPv6 multicast scope */
#define IPAT_TYPE_IPV6_MCSCOPE_LINK         0x20000000  /**< Link IPv6 multicast scope */
#define IPAT_TYPE_IPV6_MCSCOPE_REALM        0x30000000  /**< Realm IPv6 multicast scope */
#define IPAT_TYPE_IPV6_MCSCOPE_ADMIN        0x40000000  /**< Admin IPv6 multicast scope */
#define IPAT_TYPE_IPV6_MCSCOPE_SITE         0x50000000  /**< Site IPv6 multicast scope */
#define IPAT_TYPE_IPV6_MCSCOPE_ORGANIZATION 0x80000000  /**< Organization IPv6 multicast scope */
#define IPAT_TYPE_IPV6_MCSCOPE_GLOBAL       0xE0000000  /**< Global IPv6 multicast scope */
#define IPAT_TYPE_IPV6_MCSCOPE_RESERVEDF    0xF0000000  /**< Reserved IPv6 multicast scope */

/* Compare function return values */
#define IPAT_COMPARE_ERROR      (-2)                    /**< @showinitializer Error in comparison */
#define IPAT_COMPARE_LESS       (-1)                    /**< @showinitializer First item is less than second item */
#define IPAT_COMPARE_EQUAL      (0)                     /**< @showinitializer Both items are equal */
#define IPAT_COMPARE_GREATER    (1)                     /**< @showinitializer First item is greater than second item */

/* IPAT IP 'any' address macros */
#define ipat_any                "any"                   /**< @showinitializer 'Any' address for any IP family in text notation */
#define ipat_any4               "any4"                  /**< @showinitializer IPv4 'any' address in text notation */
#define ipat_any6               "any6"                  /**< @showinitializer IPv6 'any' address in text notation */

/**
 * Data type containing IP address parameters.
 */
typedef struct ipat_address_t {
    bool any;                                           /**< Indicates whether it's an 'any' address */
    int family;                                         /**< Indicates the IP family */
    unsigned char buffer[IPAT_SIZE_SAFE];               /**< Holds the actual address in binary format */
    int prefixlen;                                      /**< The address' prefix length */
} ipat_address_t;

/**
 * Data type containing an IP address.
 */
typedef struct ipat_t {
    bool valid;                                         /**< Indicates whether structure is valid */
    ipat_address_t addr;                                /**< Structure describing the actual address */
} ipat_t;

#define IPAT_INITIALIZE         { .valid = false }      /**< @showinitializer IPAT structure initialization at declaration. */

/*
 * IPAT MAC definitions
 */

#include <netinet/ether.h>

/* IPAT MAC string length macros */
#define IPAT_STRLEN_MAC         (18)                    /**< MAC address minimal string buffer size, with separator */
#define IPAT_STRLEN_MAC_COMPACT (13)                    /**< MAC address minimal string buffer size, with no separator */
#define IPAT_STRLEN_MAC_SAFE    (IPAT_STRLEN_MAC)       /**< Safe MAC address minimal string buffer size */

/* IPAT MAC storage size macros */
#define IPAT_SIZE_MAC           ((int) sizeof(struct ether_addr))   /**< MAC address storage size */
#define IPAT_SIZE_MAC_PART      (3)                                 /**< Part of MAC address storage size */
#define IPAT_SIZE_MAC_SAFE      (IPAT_SIZE_MAC)                     /**< Safe MAC address storage size */

/**
 * Data type containing a MAC address.
 */
typedef struct ipat_mac_t {
    bool valid;                                         /**< Indicates whether structure is valid */
    unsigned char buffer[IPAT_SIZE_MAC_SAFE];           /**< Holds the actual address in binary format */
} ipat_mac_t;

/** MAC address part indicators. */
typedef enum {
    ipat_mac_part_full = 0,         /**< Full MAC address */
    ipat_mac_part_oui,              /**< OUI part of the MAC address */
    ipat_mac_part_nic,              /**< NIC part of the MAC address */
} ipat_mac_part_t;

/** Supported MAC address separators. */
typedef enum {
    ipat_mac_sep_none = 0,          /**< No separator */
    ipat_mac_sep_colon,             /**< ':' separator */
    ipat_mac_sep_dash,              /**< '-' separator */
    ipat_mac_sep_underscore,        /**< '_' separator */
} ipat_mac_sep_t;

#define IPAT_MAC_INITIALIZE     { .valid = false }      /**< @showinitializer IPAT MAC structure initialization at declaration. */

/*
 * Common definitions
 */

/** Supported operations for applying masks. */
typedef enum {
    ipat_oper_and,                  /**< Perform bitwise AND operation */
    ipat_oper_or,                   /**< Perform bitwise OR operation */
    ipat_oper_xor                   /**< Perform bitwise XOR operation */
} ipat_oper_t;

/** Mask upper or lower bits indicators. */
typedef enum {
    ipat_bits_upper,                /**< Act upon the upper bits */
    ipat_bits_lower                 /**< Act upon the lower bits */
} ipat_bits_t;

#ifdef __cplusplus
}
#endif

#endif
