/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _IPAT_IPAT_MAC_H_
#define _IPAT_IPAT_MAC_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @file ipat_mac.h
 * @brief IPAT MAC functions.
 *
 * @note
 * Check ipat.h for information regarding errno.
 */

#include <ipat/ipat_def.h>

/**
 * Initialize an IPAT MAC structure.
 * Required to do before using it.
 * @param mac IPAT MAC structure
 * @return True for success
 */
bool ipat_mac_initialize(ipat_mac_t* mac);

/**
 * Clean up an IPAT MAC structure.
 * Required to do after usage.
 * @param mac IPAT MAC structure
 * @return Void
 */
void ipat_mac_cleanup(ipat_mac_t* mac);

/**
 * Copy the source to the destination.
 * @param dst IPAT MAC destination structure
 * @param src IPAT MAC source structure
 * @return True for success
 */
bool ipat_mac_copy(ipat_mac_t* dst, const ipat_mac_t* src);

/**
 * Retrieve the storage size for the part
 * of the MAC address.
 * @param part Get size for this part
 * @return Storage size or -1 upon failure
 */
int ipat_mac_max_size(ipat_mac_part_t part);

/**
 * Retrieve the minimum string buffer length
 * for a MAC address.
 * @param sep Separator used within MAC address
 * @return The minimum length or -1 upon failure
 */
int ipat_mac_min_string_length(ipat_mac_sep_t sep);

/**
 * Check whether a character is a valid MAC address
 * separator.
 * To be valid, the separator must conform to the ipat_mac_sep_t enum.
 * If valid and sep is not NULL, the resulting
 * type is stored there.
 * @param csep Character to test for separator validity
 * @param sep Separator as used
 * @return True for success
 */
bool ipat_mac_valid_separator(char csep, ipat_mac_sep_t* sep);

/**
 * Check if an IPAT MAC structure is valid.
 * This means, is there an MAC address stored and
 * can the IPAT MAC structure be used with other functions.
 * @param mac IPAT MAC structure
 * @return True for success
 */
bool ipat_mac_valid(const ipat_mac_t* mac);

/**
 * Convert a MAC address in text format
 * into an IPAT MAC structure.
 * The address can be either with or without
 * a separator within the MAC address.
 * Whether a separator is used is detected automatically.
 * The separator must conform to the ipat_mac_sep_t enum.
 * @param mac IPAT MAC structure
 * @param src The MAC address in text format
 * @return True for success
 */
bool ipat_mac_pton(ipat_mac_t* mac, const char* src);

/**
 * Convert an IPAT MAC structure into text format.
 * The result is stored in buffer dst of length dstlen,
 * which must be large enough to store the result.
 * The upper argument defines if the address should be
 * in upper-case or lower-case.
 * The separator must conform to the ipat_mac_sep_t enum.
 * @param mac IPAT MAC structure
 * @param dst Buffer to store the result
 * @param dstlen The buffer's length
 * @param sep Separator used within MAC address
 * @param upper Upper-case or lower-case
 * @return Pointer to the resulting buffer or NULL upon failure
 */
char* ipat_mac_ntop(const ipat_mac_t* mac, char* dst, int dstlen, ipat_mac_sep_t sep, bool upper);

/**
 * Convert an IPAT MAC structure into text format.
 * A sufficiently large buffer is allocated
 * on the heap, in which case it must be freed
 * by the caller after usage.
 * The upper argument defines if the address should be
 * in upper-case or lower-case.
 * The separator must conform to the ipat_mac_sep_t enum.
 * @param mac IPAT MAC structure
 * @param sep Separator used within MAC address
 * @param upper Upper-case or lower-case
 * @return Pointer to the resulting buffer or NULL upon failure
 */
char* ipat_mac_ntop_alloc(const ipat_mac_t* mac, ipat_mac_sep_t sep, bool upper);

/**
 * Retrieve a copy of the internal buffer storing
 * the MAC address in binary format.
 * This buffer must be freed after usage.
 * @param mac IPAT MAC structure
 * @return A copy of the internal buffer or NULL upon failure
 */
void* ipat_mac_buffer(const ipat_mac_t* mac);

/**
 * Retrieve a pointer to the internal buffer storing
 * the MAC address in binary format.
 * This buffer must not be altered!
 * @param mac IPAT MAC structure
 * @return A pointer to the internal buffer or NULL upon failure
 */
void* ipat_mac_da_buffer(ipat_mac_t* mac);

/**
 * Set the MAC address directly from an already
 * binary formatted source.
 * @param mac IPAT MAC structure
 * @param src The buffer to retrieve the part from
 * @param srclen The size of the buffer
 * @return True for success
 */
bool ipat_mac_set(ipat_mac_t* mac, const void* src, int srclen);

/**
 * Set the NUL MAC address directly.
 * @param mac IPAT MAC structure
 * @return True for success
 */
bool ipat_mac_set_nul(ipat_mac_t* mac);

/**
 * Retrieve a part of the MAC address in binary format.
 * The result is stored in dst, which must be large
 * enough to store the part.
 * @param mac IPAT MAC structure
 * @param dst The buffer to store the part
 * @param dstlen The size of the buffer
 * @param part The part to store
 * @return True for success
 */
bool ipat_mac_get_part(const ipat_mac_t* mac, void* dst, int dstlen, ipat_mac_part_t part);

/**
 * Set a part of the MAC address in binary format.
 * The part is retrieved from src, which must be large
 * enough to hold the part.
 * If setting the full MAC address, the IPAT MAC
 * structure becomes valid.
 * Else, the IPAT MAC structure must already be valid.
 * @param mac IPAT MAC structure
 * @param src The buffer to retrieve the part from
 * @param srclen The size of the buffer
 * @param part The part to store
 * @return True for success
 */
bool ipat_mac_set_part(ipat_mac_t* mac, const void* src, int srclen, ipat_mac_part_t part);

/**
 * Check if MAC address is a unicast or
 * multicast address.
 * @param mac IPAT MAC structure
 * @return True if unicast
 */
bool ipat_mac_is_unicast(const ipat_mac_t* mac);

/**
 * Make the MAC address a unicast or
 * multicast address.
 * @param mac IPAT MAC structure
 * @param unicast Make it unicast or multicast
 * @return True for success
 */
bool ipat_mac_set_unicast(ipat_mac_t* mac, bool unicast);

/**
 * Check if MAC address is a local or
 * global address.
 * @param mac IPAT MAC structure
 * @return True if local
 */
bool ipat_mac_is_local(const ipat_mac_t* mac);

/**
 * Make the MAC address a local or
 * global address.
 * @param mac IPAT MAC structure
 * @param local Make it local or global
 * @return True for success
 */
bool ipat_mac_set_local(ipat_mac_t* mac, bool local);

/**
 * Create a MAC mask for the given family with length len.
 * This will set either the upper or the lower bits
 * of the IPAT MAC structure.
 * @param mac IPAT MAC structure
 * @param len The length of the requested MAC mask
 * @param bits Defines which bits to set
 * @return True for success
 */
bool ipat_mac_mask_create(ipat_mac_t* mac, int len, ipat_bits_t bits);

/**
 * Apply a MAC mask to a MAC address.
 * The operation can be AND, OR or XOR.
 * @param mac IPAT MAC structure
 * @param mask IPAT MAC structure containing the MAC mask
 * @param oper Defines the operation to use
 * @return True for success
 */
bool ipat_mac_mask_apply(ipat_mac_t* mac, const ipat_mac_t* mask, ipat_oper_t oper);

/**
 * Apply a MAC mask to a MAC address directly.
 * This is a combination of ipat_mac_mask_create() and
 * ipat_mac_mask_apply().
 * @param mac IPAT MAC structure
 * @param len The length of the requested MAC mask
 * @param bits Defines which bits to set
 * @param oper Defines the operation to use
 * @return True for success
 */
bool ipat_mac_mask_apply_direct(ipat_mac_t* mac, int len, ipat_bits_t bits, ipat_oper_t oper);

/**
 * Compare 2 IPAT MAC structures.
 * The part defines the types of comparison.
 * @param a IPAT MAC structure 1
 * @param b IPAT MAC structure 2
 * @param part Type of comparison
 * @return IPAT_COMPARE_LESS, IPAT_COMPARE_EQUAL or IPAT_COMPARE_GREATER, or IPAT_COMPARE_ERROR upon failure
 */
int ipat_mac_compare(const ipat_mac_t* a, const ipat_mac_t* b, ipat_mac_part_t part);

#ifdef __cplusplus
}
#endif

#endif
